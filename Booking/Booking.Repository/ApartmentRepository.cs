﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using Booking.Repository.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Booking.Repository
{
    public class ApartmentRepository: IApartmentRepository
    {
        public ApartmentRepository() { }

        static string connectionString = "Server = localhost; Database = Booking; Trusted_Connection = True;";

        public async Task<List<IApartment>> GetAllApartmentsAsync(Sorting sort, Pager page, Filtering filter)
        {
            List<IApartment> listOfApartments = new List<IApartment>();

            StringBuilder queryString = new StringBuilder("Select * from Apartment WHERE 1=1");
            await AddFilter(filter, queryString);
            await AddSorting(sort, queryString);
            await AddPager(sort, page, queryString);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString.ToString(), connection);
                SqlDataReader reader = await command.ExecuteReaderAsync();

                while (reader.Read())
                {
                    IApartment apartment = new Apartment();
                    apartment.ID = int.Parse(reader["ID"].ToString());
                    apartment.Created = DateTime.Parse(reader["Created"].ToString());
                    apartment.Updated = DateTime.Parse(reader["Updated"].ToString());
                    apartment.Rating = double.Parse(reader["Rating"].ToString());
                    apartment.Price = int.Parse(reader["Price"].ToString());
                    apartment.Rooms = int.Parse(reader["Rooms"].ToString());
                    apartment.Free = bool.Parse(reader["Free"].ToString());
                    apartment.LandlordID = int.Parse(reader["LandlordID"].ToString());
                    apartment.AddressID = int.Parse(reader["AddressID"].ToString());
                    listOfApartments.Add(apartment);
                }

                reader.Close();
                connection.Close();
            }

            return listOfApartments;
        }

        public async Task<IApartment> GetApartmentByIdAsync(int id)
        {
            IApartment apartment = new Apartment();

            string queryString = "SELECT * FROM Apartment WHERE ID=" + id + ";";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlDataReader reader = await command.ExecuteReaderAsync();

                while (reader.Read())
                {
                    apartment.ID = int.Parse(reader["ID"].ToString());
                    apartment.Created = DateTime.Parse(reader["Created"].ToString());
                    apartment.Updated = DateTime.Parse(reader["Updated"].ToString());
                    apartment.Rating = double.Parse(reader["Rating"].ToString());
                    apartment.Price = int.Parse(reader["Price"].ToString());
                    apartment.Rooms = int.Parse(reader["Rooms"].ToString());
                    apartment.Free = bool.Parse(reader["Free"].ToString());
                    apartment.LandlordID = int.Parse(reader["LandlordID"].ToString());
                    apartment.AddressID = int.Parse(reader["AddressID"].ToString());
                }

                reader.Close();
                connection.Close();
            }

            return apartment;
        }

        public async Task SaveNewApartmentAsync(IApartment apartment)
        {
            string queryString = @"INSERT INTO Apartment (Created, Updated, Rating, Price, Rooms, Free, LandlordID, AddressID) VALUES(@Created, @Updated, @Rating, @Price, @Rooms, @Free, @LandlordID, @AddressID)";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@Created", apartment.Created);
                command.Parameters.AddWithValue("@Updated", apartment.Updated);
                command.Parameters.AddWithValue("@Rating", apartment.Rating);
                command.Parameters.AddWithValue("@Price", apartment.Price);
                command.Parameters.AddWithValue("@Rooms", apartment.Rooms);
                command.Parameters.AddWithValue("@Free", apartment.Free);
                command.Parameters.AddWithValue("@LandlordID", apartment.LandlordID);
                command.Parameters.AddWithValue("@AddressID", apartment.AddressID);

                await command.ExecuteNonQueryAsync();
                connection.Close();
            }
        }

        public async Task UpdateApartmentAsync(int id, DateTime date, double rating, int price, int rooms, bool free)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand();

                if (rating != null)
                {
                    string queryString = "UPDATE Apartment Set Rating='" + rating + "' WHERE ID='" + id + "'";
                    command = new SqlCommand(queryString, connection);
                    await command.ExecuteNonQueryAsync();
                }

                if (price != null)
                {
                    string queryString = "UPDATE Apartment Set price='" + price + "' WHERE ID='" + id + "'";
                    command = new SqlCommand(queryString, connection);
                    await command.ExecuteNonQueryAsync();
                }

                if (rooms != null)
                {
                    string queryString = "UPDATE Apartment Set Rooms='" + rooms + "' WHERE ID='" + id + "'";
                    command = new SqlCommand(queryString, connection);
                    await command.ExecuteNonQueryAsync();
                }

                if (free != true)
                {
                    string queryString = "UPDATE Apartment Set Free='" + free + "' WHERE ID='" + id + "'";
                    command = new SqlCommand(queryString, connection);
                    await command.ExecuteNonQueryAsync();
                }

                SqlCommand command2 = new SqlCommand("UPDATE Apartment SET Updated='" + date + "'  WHERE ID='" + id + "'", connection);
                await command2.ExecuteNonQueryAsync();
                connection.Close();
            }
        }

        public async Task RemoveApartmentAsync(int id)
        {
            string queryString = "DELETE FROM Apartment WHERE ID=" + id + ";";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);

                await command.ExecuteNonQueryAsync();
                connection.Close();
            }
        }

        private async Task<StringBuilder> AddFilter(Filtering filter, StringBuilder querryString)
        {

            if (filter.Price != 0)
            {
                querryString.Append($" AND Price BETWEEN '{filter.Price * 0.75}' AND '{filter.Price * 1.2}'");
            }
            if (filter.Rooms != 0)
            {
                querryString.Append($" AND Rooms = '{filter.Rooms}' ");
            }

            return await Task.FromResult(querryString);
        }

        private async Task<StringBuilder> AddSorting(Sorting sorting, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy))
            {
                queryString.Append($" ORDER BY '{sorting.OrderBy}' {sorting.SortOrder}");
            }

            return await Task.FromResult(queryString);
        }

        private async Task<StringBuilder> AddPager(Sorting sorting, Pager pager, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy) && pager.PageNumber != 0 && pager.ItemsPerPage != 0)
            {
                queryString.Append($" OFFSET {(pager.PageNumber - 1) * pager.ItemsPerPage} ROWS FETCH NEXT {pager.ItemsPerPage} ROWS ONLY;");
            }

            return await Task.FromResult(queryString);
        }
    }
}