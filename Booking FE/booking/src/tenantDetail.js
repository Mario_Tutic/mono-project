import React, {useState, useEffect} from "react";
import './tenantDetail.css';


function Tenant({match}){

    useEffect(() => {
       fetchTenant();
       console.log(match);
    },[]);

    const [tenant, setTenant]=useState({});

    const fetchTenant = async () => {
        const fetchTenant = await fetch('https://localhost:44315/api/Tenant/?Id='+match.params.id);
        const tenant = await fetchTenant.json();
        setTenant(tenant)
        console.log(match);
    };
  


        return(
      


<div class="wrapper">
    <div class="left">
        <img src="https://www.machinecurve.com/wp-content/uploads/2019/07/thispersondoesnotexist-1.jpg" alt="user" width="100"/>
       
        <h4>{tenant.Name} {tenant.Surname}</h4>
        
    </div>
    <div class="right">
        <div class="info">
            <h3>Information</h3>
            <div class="info_data">
                 <div class="data">
                    <h4>Email</h4>
                    <p>{tenant.Email}</p>
                 </div>
                 <div class="data">
                   <h4>Phone</h4>
                    <p>{tenant.PhoneNumber}</p>
              </div>
            </div>
        </div>
      
      <div class="projects">
            <h3>Bookings</h3>
            <div class="projects_data">
                 <div class="data">
                    <h4>Recent bookings</h4>
                    <p>Lorem ipsum dolor sit amet.</p>
                 </div>
                 <div class="data">
                   <h4>Most Visited</h4>
                    <p>dolor sit amet.</p>
              </div>
            </div>
        </div>
      
        
    </div>
</div>
        )
    }


export default Tenant;