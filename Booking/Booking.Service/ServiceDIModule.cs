﻿using Booking.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;


namespace Booking.Service
{
    public class ServiceDIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BookingService>()
                       .As<IBookingService>()
                       .InstancePerRequest();

            builder.RegisterType<TenantService>()
                       .As<ITenantService>()
                       .InstancePerRequest();
           
            builder.RegisterType<ApartmentService>()
                       .As<IApartmentService>()
                       .InstancePerRequest();

            builder.RegisterType<LandlordService>()
                      .As<ILandlordService>()
                      .InstancePerRequest();

            builder.RegisterType<ReviewService>()
                      .As<IReviewService>()
                      .InstancePerRequest();
                      
            builder.RegisterType<AddressService>()
                      .As<IAddressService>()
                      .InstancePerRequest();
        }
    }
}