﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using Booking.Repository.Common;

namespace Booking.Repository
{
    public class BookingRepository : IBookingRepository
    {
        public async Task<List<IOnlineBooking>> GetAllBookingsAsync(Sorting sort, Pager page, Filtering filter)
        {
            SqlConnection connection = new SqlConnection("data source=.\\SQLEXPRESS;Database=Booking;integrated security=SSPI");
            SqlCommand cmd = null;
            StringBuilder commandString = new StringBuilder("Select * from Booking WHERE 1=1");
            try
            {
                await AddFilter(filter, commandString);
                await AddSorting(sort, commandString);
                await AddPager(sort, page, commandString);
                cmd = new SqlCommand(commandString.ToString(), connection);
                connection.Open();
                SqlDataReader reader = await cmd.ExecuteReaderAsync();
                List<IOnlineBooking> bookings = new List<IOnlineBooking>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var tempBooking = new OnlineBooking();
                        tempBooking.ID = Convert.ToInt32(reader["ID"]);
                        tempBooking.Created = Convert.ToDateTime(reader["Created"]);
                        tempBooking.Updated = Convert.ToDateTime(reader["Updated"]);
                        tempBooking.BookInDate = Convert.ToDateTime(reader["BookInDate"]);
                        tempBooking.BookOutDate = Convert.ToDateTime(reader["BookOutDate"]);
                        tempBooking.ApartmentID = Convert.ToInt32(reader["ApartmentID"]);
                        tempBooking.TenantID = Convert.ToInt32(reader["TenantID"]);
                        bookings.Add(tempBooking);
                    }
                    reader.Close();
                    connection.Close();
                    return bookings;
                }
                else
                {
                    Console.WriteLine("No rows found.");
                    reader.Close();
                    connection.Close();
                    return (bookings);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public async Task<IOnlineBooking> GetBookingByIdAsync(int id)
        {
            SqlConnection connection = new SqlConnection("data source=.\\SQLEXPRESS;Database=Booking;integrated security=SSPI");
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                "SELECT * FROM Booking WHERE ID="+id, connection);
                SqlDataReader reader = await command.ExecuteReaderAsync();
                OnlineBooking booking = new OnlineBooking();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        booking.ID = Convert.ToInt32(reader["ID"]);
                        booking.Created = Convert.ToDateTime(reader["Created"]);
                        booking.Updated = Convert.ToDateTime(reader["Updated"]);
                        booking.BookInDate = Convert.ToDateTime(reader["BookInDate"]);
                        booking.BookOutDate = Convert.ToDateTime(reader["BookOutDate"]);
                        booking.ApartmentID = Convert.ToInt32(reader["ApartmentID"]);
                        booking.TenantID = Convert.ToInt32(reader["TenantID"]);
                    }
                    reader.Close();
                    connection.Close();
                    return booking;
                }
                else
                {
                    Console.WriteLine("No rows found.");
                    reader.Close();
                    connection.Close();
                    return null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public async Task DeleteBookingAsync(int id)
        {
            try
            {
                SqlConnection connection = new SqlConnection("data source=.\\SQLEXPRESS;Database=Booking;integrated security=SSPI");
                connection.Open();
                SqlCommand command = new SqlCommand("DELETE FROM Booking WHERE ID=" + id + ";", connection);
                await command.ExecuteNonQueryAsync();
                connection.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public async Task PostNewBookingAsync(IOnlineBooking NewBooking)
        {
            try
            {
                SqlConnection connection = new SqlConnection("data source=.\\SQLEXPRESS;Database=Booking;integrated security=SSPI");
                connection.Open();
                SqlCommand command = new SqlCommand("INSERT INTO Booking VALUES(CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,'"+ NewBooking.BookInDate.ToString("MM/dd/yyyy HH:mm:ss") + "','" + NewBooking.BookOutDate.ToString("MM/dd/yyyy HH:mm:ss") + "'," + NewBooking.ApartmentID + "," + NewBooking.TenantID + ")", connection);
                await command.ExecuteNonQueryAsync();
                connection.Close();
                return;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public async Task PutBookingAsync(IOnlineBooking NewBooking,int ID)
        {
            try
            {
                SqlConnection connection = new SqlConnection("data source=.\\SQLEXPRESS;Database=Booking;integrated security=SSPI");
                connection.Open();
                SqlCommand command = new SqlCommand("UPDATE Booking SET Updated = CURRENT_TIMESTAMP, BookInDate ='"+ NewBooking.BookInDate.ToString("MM/dd/yyyy HH:mm:ss") + "', BookOutDate= '"+ NewBooking.BookOutDate.ToString("MM/dd/yyyy HH:mm:ss") +"',ApartmentID="+ NewBooking.ApartmentID + ",TenantID="+ NewBooking.TenantID + "WHERE ID=" + ID + "; ", connection);
                await command.ExecuteNonQueryAsync();
                connection.Close();
                return;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        private async Task<StringBuilder> AddFilter(Filtering filter, StringBuilder commandString)
        {

            if (filter.BookInDate != DateTime.MinValue)
            {
                commandString.Append($" AND (CAST(BookInDate as date) = '{filter.BookInDate.ToString("MM/dd/yyyy")}')");
            }
            if (filter.BookOutDate!= DateTime.MinValue)
            {
                commandString.Append($" AND (CAST(BookOutDate as date) = '{filter.BookInDate.ToString("MM/dd/yyyy")}')");
            }
            if (filter.ApartmentID!=0)
            {
                commandString.Append($" AND (ApartmentID = '{filter.ApartmentID}')");
            }
            if (filter.TenantID!=0)
            {
                commandString.Append($" AND (TenantID = '{ filter.TenantID}') ");
            }
            return await Task.FromResult(commandString);
        }
        private async Task<StringBuilder> AddSorting(Sorting sorting, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy))
            {
                queryString.Append($" ORDER BY '{sorting.OrderBy}' {sorting.SortOrder}");
            }
            return await Task.FromResult(queryString);
        }
        private async Task<StringBuilder> AddPager(Sorting sorting, Pager pager, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy) && pager.PageNumber != 0 && pager.ItemsPerPage != 0)
            {
                queryString.Append($" OFFSET {(pager.PageNumber - 1) * pager.ItemsPerPage} ROWS FETCH NEXT {pager.ItemsPerPage} ROWS ONLY;");
            }
            return await Task.FromResult(queryString);
        }


    }
}
