﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using Booking.Repository.Common;
using Booking.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Service
{
    public class TenantService : ITenantService
    {
        protected ITenantRepository TenantRepository { get; private set; }
        public TenantService(ITenantRepository repository) { TenantRepository = repository; }

        public async Task<int> GetTenantCount()
        {
            return await TenantRepository.GetTenantCount();
        }
        public async Task<List<ITenant>> GetAllTenantsAsync(Sorting sort, Pager page, Filtering filter)
        {
          return  await TenantRepository.GetAllTenantsAsync(sort,page,filter);
        }
        public async Task<ITenant> GetTenantByIdAsync(int id)
        {
            return await TenantRepository.GetTenantByIdAsync(id);
        }
        public async Task RemoveTenantAsync(int id)
        {
             await TenantRepository.RemoveTenantAsync(id);
        }
        public async Task SaveNewTenantAsync(ITenant tenant)
        {
            tenant.Created = DateTime.UtcNow;
            tenant.Updated = DateTime.UtcNow;

            await TenantRepository.SaveNewTenantAsync(tenant);
        }
        public async Task UpdateTenantAsync(int id, string phoneNumber, string email, string password)
        {
            DateTime date= DateTime.UtcNow;

            await TenantRepository.UpdateTenantAsync(id, date, phoneNumber, email, password);
        }
    }
}
