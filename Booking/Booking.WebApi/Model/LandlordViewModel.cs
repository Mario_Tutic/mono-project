﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking.WebApi.Model
{
    public class LandlordViewModel
    {
        public int Id { set; get; }
        public DateTime Created { set; get; }
        public DateTime Updated { set; get; }
        public string Name { set; get; }
        public string Surname { set; get; }
        public string PhoneNumber { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
    }
}