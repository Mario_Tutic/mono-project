﻿using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Model
{
    public class Address : IAddress
    {
        public int ID { set; get; }
        public DateTime Created { set; get; } = DateTime.UtcNow;
        public DateTime Updated { set; get; } = DateTime.UtcNow;
        public string Country { set; get; }
        public string City { set; get; }
        public string Street { set; get; }
        public int House { set; get; }
        public int Flat { set; get; }
    }
}
