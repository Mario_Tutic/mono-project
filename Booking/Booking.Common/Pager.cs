﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Common
{
    public class Pager
    {
        public int ItemsPerPage { get; set; }

        public int PageNumber { get; set; }
    }
}
