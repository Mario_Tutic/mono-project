import './App.css';
import Tenants from './Tenants';
import Landlords from './Landlords';
import Nav from './Nav';
import Tenant from './tenantDetail';
import Landlord from './landlordDetail';
import {BrowserRouter as Router, Switch,Route} from 'react-router-dom';

//bookings
import ListBookingComponent from './components/ListBookingComponent';
import UpdateBookingComponent from './components/UpdateBookingComponent';
import ViewBookingComponent from './components/ViewBookingComponent';
import CreateBookingComponent from './components/CreateBookingComponent';
//reviews
import ListReviewsComponent from './reviewComponents/ListReviewsComponent';
import CreateReviewComponent from './reviewComponents/CreateReviewComponent';
import UpdateReviewComponent from './reviewComponents/UpdateReviewComponent';



function App() {
  return (
    
      <Router>
          <div className="App">
            <Nav />
            <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/tenants" exact component={Tenants} />
            <Route path="/landlords" exact component={Landlords} />
            <Route path="/tenants/:id" component={Tenant}/>
            <Route path="/landlords/:id" component={Landlord}/>

            <Route path = "/bookings" component = {ListBookingComponent}></Route>
            <Route path = "/add-booking" component = {CreateBookingComponent}></Route>
            <Route path = "/view-booking/:id" component = {ViewBookingComponent}></Route>
            <Route path = "/update-booking/:id" component = {UpdateBookingComponent}></Route>

            <Route path = "/reviews" component = {ListReviewsComponent}></Route>
            <Route path = "/add-review" component = {CreateReviewComponent}></Route>
            <Route path = "/update-review/:id" component = {UpdateReviewComponent}></Route>
            </Switch>
          </div>  
      </Router>
  );
}

const Home = () =>(
  <div>
    <h1>Home page</h1>
  </div>
)

export default App;
