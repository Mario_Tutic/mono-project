﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Model.Common
{
    public interface IApartment
    {
        int ID { set; get; }
        DateTime Created { set; get; }
        DateTime Updated { set; get; }
        double Rating { set; get; }
        int Price { set; get; }
        int Rooms { set; get; }
        bool Free { set; get; }
        int LandlordID { set; get; }
        int AddressID { set; get; }
        IAddress Address { set; get; }
        ILandlord Landlord { set; get; }
    }
}
