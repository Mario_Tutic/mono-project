﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking.WebApi.Model
{
    public class OnlineBookingViewModel
    {
        public DateTime BookInDate { set; get; }
        public DateTime BookOutDate { set; get; }
        public int ApartmentID { set; get; }
        public int TenantID { set; get; }
    }
}
