﻿using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Model
{
    public class Review : IReview
    {
        public int ID { set; get; }
        public DateTime Created { set; get; }
        public DateTime Updated { set; get; }
        public string ReviewText { set; get; }
        public int TenantID { set; get; }
        public int ApartmentID { set; get; }
    }
}
