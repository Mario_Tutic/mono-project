﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Common
{
    public class Filtering
    {
        public string FilterWord { get; set; }

        public string FilterBy { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public int Price { set; get; }

        public int Rooms { set; get; }

        public DateTime BookInDate { set; get; }

        public DateTime BookOutDate { set; get; }

        public int ApartmentID { set; get; }

        public int TenantID { set; get; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Street { get; set; }
    }
}
