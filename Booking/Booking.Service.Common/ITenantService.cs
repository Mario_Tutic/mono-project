﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Service.Common
{
    public interface ITenantService
    {
        Task<List<ITenant>> GetAllTenantsAsync(Sorting sort, Pager page, Filtering filter);
        Task<ITenant> GetTenantByIdAsync(int id);
        Task RemoveTenantAsync(int id);
        Task SaveNewTenantAsync(ITenant tenant);
        Task UpdateTenantAsync(int id, string phoneNumber, string email, string password);

        Task<int> GetTenantCount();
    }
}
