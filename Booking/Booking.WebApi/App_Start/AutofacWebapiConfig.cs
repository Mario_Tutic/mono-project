﻿using AutoMapper.Contrib.Autofac.DependencyInjection;
using Booking.Model;
using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using System.Reflection;
using System.Web.Http;
using System;
using Booking.Service;
using Booking.Repository;
using Autofac.Integration.Mvc;

namespace Booking.WebApi.App_Start
{
    public class AutoFacWebapiConfig
    {
        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
        
        public static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterModule(new ServiceDIModule());
            builder.RegisterModule(new RepositoryDIModule());
            builder.RegisterModule(new ModelDIModule());

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterAutoMapper(typeof(AutoMapperProfile).Assembly);

            Container = builder.Build();
            return Container;
        }
    }
}