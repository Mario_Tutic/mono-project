﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Model.Common
{
    public interface IOnlineBooking
    {
        int ID { set; get; }
        DateTime Created { set; get; }
        DateTime Updated { set; get; }
        DateTime BookInDate { set; get; }
        DateTime BookOutDate { set; get; }
        int ApartmentID { set; get; }
        int TenantID { set; get; }
    }
}
