import React, { Component } from 'react'
import BookingService from '../services/BookingService';

class CreateBookingComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            BookInDate: '',
            BookOutDate: '',
            ApartmentID: '',
            TenantID: '',
        }
        this.changeBookInDateHandler = this.changeBookInDateHandler.bind(this);
        this.changeBookOutDateHandler = this.changeBookOutDateHandler.bind(this);
        this.changeApartmentIDHandler = this.changeApartmentIDHandler.bind(this);
        this.changeTenantIDHandler = this.changeTenantIDHandler.bind(this);
    }

    // step 3
    componentDidMount(){

        // step 4
        if(this.state.id === '_add'){
            return
        }else{
            BookingService.getBookingById(this.state.id).then( (res) =>{
                let booking = res.data;
                this.setState({BookInDate: booking.BookInDate,
                    BookOutDate: booking.BookOutDate,
                    ApartmentID: booking.ApartmentID,
                    TenantID: booking.TenantID
                });
            });
        }        
    }
    saveNewEBooking = (e) => {
        e.preventDefault();
        let booking = {BookInDate: this.state.BookInDate, BookOutDate: this.state.BookOutDate,ApartmentID: this.state.ApartmentID,TenantID: this.state.TenantID};
        console.log('booking => ' + JSON.stringify(booking));

        BookingService.createBooking(booking).then(res =>{
            this.props.history.push('/bookings');
        });
    }
    
    changeBookInDateHandler= (event) => {
        this.setState({BookInDate: event.target.value});
    }

    changeBookOutDateHandler= (event) => {
        this.setState({BookOutDate: event.target.value});
    }
    changeApartmentIDHandler= (event) => {
        this.setState({ApartmentID: event.target.value});
    }
    changeTenantIDHandler= (event) => {
        this.setState({TenantID: event.target.value});
    }

    cancel(){
        this.props.history.push('/bookings');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Booking</h3>
        }else{
            return <h3 className="text-center">Update Booking</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> BookInDate: </label>
                                            <input placeholder="Book In Date" name="bookInDate" className="form-control" 
                                                value={this.state.bookInDate} onChange={this.changeBookInDateHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> BookOutDate: </label>
                                            <input placeholder="Book Out Date" name="bookOutDate" className="form-control" 
                                                value={this.state.bookOutDate} onChange={this.changeBookOutDateHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> ApartmentID: </label>
                                            <input placeholder="ApartmentID" name="apartmentID" className="form-control" 
                                                value={this.state.apartmentID} onChange={this.changeApartmentIDHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> TenantID: </label>
                                            <input placeholder="TenantID" name="tenantID" className="form-control" 
                                                value={this.state.tenantID} onChange={this.changeTenantIDHandler}/>
                                        </div>
                                        <button className="btn btn-success" onClick={this.saveNewEBooking}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default CreateBookingComponent
