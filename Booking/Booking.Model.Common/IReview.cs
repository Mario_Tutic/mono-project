﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Model.Common
{
    public interface IReview
    {
        int ID { set; get; }
        DateTime Created { set; get; }
        DateTime Updated { set; get; }
        string ReviewText { set; get; }
        int TenantID { set; get; }
        int ApartmentID { set; get; }
    }
}
