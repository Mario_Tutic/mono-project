import React, { Component } from 'react'
import BookingService from '../services/BookingService';

class UpdateBookingComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            apartmentID:'',
            tenantID:'',
            BookInDate: '',
            BookOutDate: '',
        }
        this.changeBookInDateHandler = this.changeBookInDateHandler.bind(this);
        this.changeBookOutDateHandler = this.changeBookOutDateHandler.bind(this);
    }

    componentDidMount(){
        BookingService.getBookingById(this.state.id).then( (res) =>{
            let TempNewBooking = res.data;
            this.setState({BookInDate: TempNewBooking.BookInDate,
                BookOutDate: TempNewBooking.BookOutDate,
                apartmentID : TempNewBooking.ApartmentID,
                tenantID : TempNewBooking.TenantID
            });
        });
    }

    updateBooking = (e) => {
        e.preventDefault();
        let TempNewBooking = {BookInDate: this.state.BookInDate, BookOutDate: this.state.BookOutDate,ApartmentID: this.state.apartmentID, TenantID: this.state.tenantID};
        console.log('TempNewBooking => ' + JSON.stringify(TempNewBooking));
        console.log('id => ' + JSON.stringify(this.state.id));
        BookingService.updateBooking(TempNewBooking, this.state.id).then( res => {
            this.props.history.push('/bookings');
        });
    }
    
    changeBookInDateHandler= (event) => {
        this.setState({BookInDate: event.target.value});
    }

    changeBookOutDateHandler= (event) => {
        this.setState({BookOutDate: event.target.value});
    }

    cancel(){
        this.props.history.push('/bookings');
    }

    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                <h3 className="text-center">Update Booking</h3>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> BookInDate: </label>
                                            <input placeholder="BookInDate" name="bookInDate" className="form-control" 
                                                value={this.state.BookInDate} onChange={this.changeBookInDateHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> BookOutDate: </label>
                                            <input placeholder="BookOutDate" name="bookInDate" className="form-control" 
                                                value={this.state.BookOutDate} onChange={this.changeBookOutDateHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.updateBooking}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default UpdateBookingComponent
