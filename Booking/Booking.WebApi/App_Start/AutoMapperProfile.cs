﻿using Booking.WebApi.Model;
using Booking.Model.Common;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using System.Reflection;
using System.Web.Http;
using System;
using Booking.Service;
using Booking.Repository;
using Autofac.Integration.Mvc;
using Booking.Model.Common;

namespace Booking.WebApi.App_Start
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<IApartment, ApartmentViewModel>();
            CreateMap<ApartmentViewModel, IApartment>();

            CreateMap<IAddress, AddressViewModel>();
            CreateMap<AddressViewModel, IAddress>();

            CreateMap<ITenant, TenantViewModel>();
            CreateMap<TenantViewModel, ITenant>();

            CreateMap<ILandlord, LandlordViewModel>();
            CreateMap<LandlordViewModel, ILandlord>();

            CreateMap<ILandlord, LandlordPersonalViewModel>();
            CreateMap<LandlordPersonalViewModel, ILandlord>();

            CreateMap<IOnlineBooking, OnlineBookingViewModel>();
            CreateMap<OnlineBookingViewModel, IOnlineBooking>();

            CreateMap<IReview, ReviewViewModel>();
            CreateMap<ReviewViewModel, IReview>();
        }
    }
}