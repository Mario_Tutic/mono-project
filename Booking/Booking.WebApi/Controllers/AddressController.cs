﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using Booking.Service;
using Booking.Service.Common;
using Booking.WebApi.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;


namespace Booking.WebApi.Controllers
{
    public class AddressController : ApiController
    {
        protected IAddressService service { get; private set; }
        protected IMapper mapper { get; private set; }

        public AddressController(IAddressService _service, IMapper _mapper)
        {
            service = _service;
            mapper = _mapper;
        }

        public Sorting sort = new Sorting();
        public Pager page = new Pager();
        public Filtering filter = new Filtering();

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllAddressesAsync(string sortBy = "", string orderBy = "", int itemsPerPage = 0, int pageNumber = 0, string country = "", string city = "", string street = "")
        {
            if (sortBy != "" || orderBy != "")
            {
                sort = new Sorting();
                sort.OrderBy = orderBy;
                sort.SortOrder = sortBy;
            }

            if (pageNumber != 0 || itemsPerPage != 0)
            {
                page = new Pager();
                page.ItemsPerPage = itemsPerPage;
                page.PageNumber = pageNumber;
            }

            if (country != "" || city != "" || street != "")
            {
                filter.Country = country;
                filter.City = city;
                filter.Street = street;
            }
            
            List<IAddress> addressList = await service.GetAllAddressesAsync(sort, page, filter);

            if (addressList != null)
            {
                List<AddressViewModel> addressViewList = new List<AddressViewModel>();

                foreach (Address address in addressList)
                {
                    var addressViewModel = mapper.Map<AddressViewModel>(address);
                    addressViewList.Add(addressViewModel);
                }

                return Request.CreateResponse(HttpStatusCode.OK, addressViewList);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, "There are no such apartmants currently listed");
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAddressByIdAsync(int id)
        {
            IAddress address = await service.GetAddressByIdAsync(id);

            if (address.Country != null)
            {
                var addressViewModel = mapper.Map<AddressViewModel>(address);

                return Request.CreateResponse(HttpStatusCode.OK, addressViewModel);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, "There is no such Apartment on that address");
        }


        [HttpPost]
        public async Task<HttpResponseMessage> SaveNewAddressAsync([FromBody] AddressViewModel addressViewModel)
        {
            List<IAddress> addressList = await service.GetAllAddressesAsync(sort, page, filter);
            IAddress newAddress = addressList.Where(s => s == addressViewModel).FirstOrDefault();

            if (newAddress.Country != null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "This apartment already exists in database");

            IAddress address = new Address();

            address = mapper.Map<IAddress>(addressViewModel);

            await service.SaveNewAddressAsync(address);
            return Request.CreateResponse(HttpStatusCode.OK, "Address for an apartment has been created");
        }

        [HttpPut]
        public async Task<HttpResponseMessage> UpdateAddressAsync(int id, string country = "", string city = "", string street = "", int house = 0, int flat = 0)
        {
            IAddress address = await service.GetAddressByIdAsync(id);

            if (address.Country != null)
            {
                await service.UpdateAddressAsync(id, country, city, street, house, flat);
                return Request.CreateResponse(HttpStatusCode.OK, "Adress of the apartment has been updated");
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, "There is no such Apartment on that address");
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> RemoveAddressAsync(int id)
        {
            IAddress address = await service.GetAddressByIdAsync(id);

            if (address.Country != null)
                return Request.CreateResponse(HttpStatusCode.OK, "Apartment on that adress has been deleted!");

            return Request.CreateResponse(HttpStatusCode.NotFound, "There is no such Apartment on that address");
        }
    }
}