import React, {useState, useEffect} from "react";
import ReactDOM from "react-dom";
import {Table, Button} from "reactstrap";
import 'react-dropdown/style.css';
import {Link} from "react-router-dom";
import axios from 'axios';
import Pagination from '@material-ui/lab/Pagination';

function Tenants(){

   
    const [tenants, setTenants]=useState([]);
    const[itemsPerPage, setItemsPerPage]=useState(3);
    const[pageNumber,setPageNumber]=useState(1);
    const[tenantsCount,setCount]=useState(0);

    useEffect(() => {
        fetchTenants();
        getTenantCount();
    },[itemsPerPage,pageNumber]);

    
    
    const getTenantCount=async()=>{
        let data= await fetch('https://localhost:44315/api/Tenant/?i=1');
        let datajson = await data.json();
        setCount(datajson);
        console.log(tenantsCount)
    }

    const fetchTenants = async() =>{
    let data= await fetch('https://localhost:44315/api/Tenant/?sortBy=ASC&orderBy=Id&itemsPerPage='+itemsPerPage+'&pageNumber='+pageNumber);
    let tenantsjson = await data.json();
    setTenants(tenantsjson); };
    
    function deleteTenant(id){
        axios.delete('https://localhost:44315/api/Tenant/'+id).then(()=>{
            fetchTenants();
        })
    }
   
    const AddLink={
        float:'right',
        marginRight: 10,
    }
  
    const navStyle={
        color: 'rgb(73, 72, 72)',
        textDecoration: 'none'
    };

    var tenantsList = tenants.map(user => {
        return (
          <tr key={user.Id}>
            <td>{user.Id}</td>
            
            <td><Link style={navStyle} to={`/tenants/${user.Id}`}>{user.Name} </Link></td>
           
            
            <td><Link style={navStyle} to={`/tenants/${user.Id}`}>{user.Surname}</Link></td>
            
            <td>{user.PhoneNumber}</td>
            <td>{user.Email}</td>
            <td>
                <Button color="success" size="sm" >Edit</Button>
                <Button onClick={() =>{deleteTenant(user.Id)}} color="danger" size="sm">Delete</Button>
            </td>            
          </tr>
        )
      });


       
      function handleChange(event,value){   
        setPageNumber(value)
      }
      
     
     
      
      function setItemsPerPageChangeHandle(event){
        setItemsPerPage(parseInt(event.target.value))
      }
      function setNumberOfPages(){
          if (tenantsCount%itemsPerPage) return parseInt(tenantsCount/itemsPerPage)+1
          else return parseInt(tenantsCount/itemsPerPage)
      }
     

        return(
            <div className="Tenants">
              
                 <h2>Tenants:</h2>
                <Table striped bordered>
                <thead>
                <tr >
                        <th>Id</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                   {tenantsList}
                 
                </tbody>       
           
                </Table>
                <div >
                <Link style={AddLink}to="/AddNewTenant"><button type="button" color="primary"> Add new Tenant</button> </Link> 
                </div>
                <div>
                <label for="Tenants">Tenants per page:  </label>
                    <select  value={itemsPerPage} onChange={setItemsPerPageChangeHandle} name="Tenants" id="TenantsPager" >
                        <option select="selected" value="3" name = "3" id="option1">3</option>
                        <option value="6" name = "6" id="option2">6</option>
                        <option value="9"name = "9" id="option3">9</option>
                    </select>
                    
          </div>
          <div>
          <div style={{ display: 'block', padding: 30 }}>
      
          <Pagination  count={setNumberOfPages()} defaultPage={1} page={pageNumber} onChange={handleChange} />
      </div>
          </div>
            </div>
            
        )
        
    }



export default Tenants;