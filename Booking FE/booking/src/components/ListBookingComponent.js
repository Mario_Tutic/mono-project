import React, { Component } from 'react'
import BookingService from '../services/BookingService'

class ListBookingComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                bookings: []
        }
        this.addBooking = this.addBooking.bind(this);
        this.editBooking = this.editBooking.bind(this);
        this.deleteBooking = this.deleteBooking.bind(this);
    }

    deleteBooking(id){
        BookingService.deleteBooking(id).then( res => {
            this.setState({bookings: this.state.bookings.filter(booking => booking.id !== id)});
            window.location.reload(false);
        });
    }
    viewBooking(id){
        this.props.history.push(`/view-booking/${id}`);
    }
    editBooking(id){
        this.props.history.push(`/update-booking/${id}`);
    }

    componentDidMount(){
        BookingService.getBookings().then((res) => {
            this.setState({ bookings: res.data});
        });
    }

    addBooking(){
        this.props.history.push('/add-booking');
    }

    render() {
        return (
            <div>
                 <h2 className="text-center">Bookings List</h2>
                 <div className = "row">
                    <button className="btn btn-primary" onClick={this.addBooking}> Book Accomodation</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> ID</th>
                                    <th> Book In Date</th>
                                    <th> Book Out Date</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.bookings.map(
                                        booking => 
                                        <tr key = {booking.ID}>
                                             <td> {booking.ID} </td>   
                                             <td> {booking.BookInDate}</td>
                                             <td> {booking.BookOutDate}</td>
                                             <td>
                                                 <button onClick={ () => this.editBooking(booking.ID)} className="btn btn-info">Update </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteBooking(booking.ID)} className="btn btn-danger">Delete </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewBooking(booking.ID)} className="btn btn-info">View </button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                 </div>

            </div>
        )
    }
}

export default ListBookingComponent
