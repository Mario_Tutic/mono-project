﻿using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Model
{
    public class Apartment : IApartment
    {
        public int ID { set; get; }
        public DateTime Created { set; get; } = DateTime.UtcNow;
        public DateTime Updated { set; get; } = DateTime.UtcNow;
        public double Rating { set; get; }
        public int Price { set; get; }
        public int Rooms { set; get; }
        public bool Free { set; get; }
        public int LandlordID { set; get; }
        public int AddressID { set; get; }
        public IAddress Address { set; get; }
        public ILandlord Landlord { set; get; }
    }
}