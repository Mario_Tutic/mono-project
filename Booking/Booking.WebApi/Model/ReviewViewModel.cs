﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking.WebApi.Model
{
    public class ReviewViewModel
    {
        public string ReviewText { set; get; }
        public int TenantID { set; get; }
        public int ApartmentID { set; get; }
    }
}