﻿using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;


namespace Booking.Model
{
    public class ModelDIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Address>()
                       .As<IAddress>()
                       .InstancePerRequest();
            builder.RegisterType<Apartment>()
                       .As<IApartment>()
                       .InstancePerRequest();
            builder.RegisterType<Landlord>()
                       .As<ILandlord>()
                       .InstancePerRequest();
            builder.RegisterType<OnlineBooking>()
                       .As<IOnlineBooking>()
                       .InstancePerRequest();
            builder.RegisterType<Review>()
                       .As<IReview>()
                       .InstancePerRequest();
            builder.RegisterType<Tenant>()
                       .As<ITenant>()
                        .InstancePerRequest();
        }
    }
}