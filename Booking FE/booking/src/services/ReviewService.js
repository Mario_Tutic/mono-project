import axios from 'axios';

const BOOKING_API_URL = "https://localhost:44315/api/review";

class ReviewService {

    getReviews(){
        return axios.get(BOOKING_API_URL);
    }

    createReview(review){
        return axios.post(BOOKING_API_URL, review);
    }

    getReviewById(reviewId){
        return axios.get(BOOKING_API_URL + '/' + reviewId);
    }

    updateReview(review, reviewId){
        return axios.put(BOOKING_API_URL + '?id=' + reviewId+'&NewReviewText='+ review);
    }

    deleteReview(reviewId){
        return axios.delete(BOOKING_API_URL + '/' + reviewId);
    }
}

export default new ReviewService()