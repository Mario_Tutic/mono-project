﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using Booking.Repository.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Data.SqlClient;
using System.Data;
using System.Net;

namespace Booking.Repository
{
    public class AddressRepository : IAddressRepository
    {
        public AddressRepository() { }

        static string connectionString = "Server = localhost; Database = Booking; Trusted_Connection = True;";

        public async Task<List<IAddress>> GetAllAddressesAsync(Sorting sort, Pager page, Filtering filter)
        {
            List<IAddress> listOfAddresses = new List<IAddress>();

            StringBuilder queryString = new StringBuilder("Select * from Address WHERE 1=1");
            await AddFilter(filter, queryString);
            await AddSorting(sort, queryString);
            await AddPager(sort, page, queryString);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString.ToString(), connection);
                SqlDataReader reader = await command.ExecuteReaderAsync();

                while (reader.Read())
                {
                    IAddress address = new Address();
                    address.ID = int.Parse(reader["ID"].ToString());
                    address.Created = DateTime.Parse(reader["Created"].ToString());
                    address.Updated = DateTime.Parse(reader["Updated"].ToString());
                    address.Country = (reader["Country"].ToString());
                    address.City = (reader["City"].ToString());
                    address.Street = (reader["Street"].ToString());
                    address.House = int.Parse(reader["House"].ToString());
                    address.Flat = int.Parse(reader["Flat"].ToString());
                    listOfAddresses.Add(address);
                }

                reader.Close();
                connection.Close();
            }

            return listOfAddresses;
        }

        public async Task<IAddress> GetAddressByIdAsync(int id)
        {
            IAddress address = new Address();

            string queryString = "SELECT * FROM Address WHERE ID=" + id + ";";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlDataReader reader = await command.ExecuteReaderAsync();

                while (reader.Read())
                {
                    address.ID = int.Parse(reader["ID"].ToString());
                    address.Created = DateTime.Parse(reader["Created"].ToString());
                    address.Updated = DateTime.Parse(reader["Updated"].ToString());
                    address.Country = (reader["Country"].ToString());
                    address.City = (reader["City"].ToString());
                    address.Street = (reader["Street"].ToString());
                    address.House = int.Parse(reader["House"].ToString());
                    address.Flat = int.Parse(reader["Flat"].ToString());
                }

                reader.Close();
                connection.Close();
            }

            return address;
        }
        public async Task SaveNewAddressAsync(IAddress address)
        {
            string queryString = @"INSERT INTO Address (Created, Updated, Country, City, Street, House, Flat) VALUES(@Created, @Updated, @Country, @City, @Street, @House, @Flat)";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);

                command.Parameters.AddWithValue("@Created", address.Created);
                command.Parameters.AddWithValue("@Updated", address.Updated);
                command.Parameters.AddWithValue("@Country", address.Country);
                command.Parameters.AddWithValue("@City", address.City);
                command.Parameters.AddWithValue("@Street", address.Street);
                command.Parameters.AddWithValue("@House", address.House);
                command.Parameters.AddWithValue("@Flat", address.Flat);

                await command.ExecuteNonQueryAsync();
                connection.Close();
            }
        }

        public async Task UpdateAddressAsync(int id, DateTime date, string country, string city, string street, int house, int flat)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand();

                if (country != "")
                {
                    string queryString = "UPDATE Address Set Country='" + country + "' WHERE ID='" + id + "'";
                    command = new SqlCommand(queryString, connection);
                    await command.ExecuteNonQueryAsync();
                }

                if (city != "")
                {
                    string queryString = "UPDATE Address Set City='" + city + "' WHERE ID='" + id + "'";
                    command = new SqlCommand(queryString, connection);
                    await command.ExecuteNonQueryAsync();
                }

                if (street != "")
                {
                    string queryString = "UPDATE Address Set Street='" + street + "' WHERE ID='" + id + "'";
                    command = new SqlCommand(queryString, connection);
                    await command.ExecuteNonQueryAsync();
                }

                if (house != null)
                {
                    string queryString = "UPDATE Address Set House='" + house + "' WHERE ID='" + id + "'";
                    command = new SqlCommand(queryString, connection);
                    await command.ExecuteNonQueryAsync();
                }

                if (flat != null)
                {
                    string queryString = "UPDATE Address Set Flat='" + flat + "' WHERE ID='" + id + "'";
                    command = new SqlCommand(queryString, connection);
                    await command.ExecuteNonQueryAsync();
                }

                SqlCommand command2 = new SqlCommand("UPDATE Address SET Updated='" + date + "'  WHERE ID='" + id + "'", connection);
                await command2.ExecuteNonQueryAsync();
                connection.Close();
            }
        }

        public async Task RemoveAddressAsync(int id)
        {
            string queryString = "DELETE FROM Address WHERE ID=" + id + ";";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);

                await command.ExecuteNonQueryAsync();
                connection.Close();
            }
        }

        private async Task<StringBuilder> AddFilter(Filtering filter, StringBuilder querryString)
        {

            if (!string.IsNullOrWhiteSpace(filter.Country))
            {
                querryString.Append($" AND Country LIKE '%{filter.Country}%'");
            }
            if (!string.IsNullOrWhiteSpace(filter.City))
            {
                querryString.Append($" AND City LIKE '{filter.City}' ");
            }
            if (!string.IsNullOrWhiteSpace(filter.Street))
            {
                querryString.Append($" AND Street LIKE '{filter.Street}' ");
            }

            return await Task.FromResult(querryString);
        }

        private async Task<StringBuilder> AddSorting(Sorting sorting, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy))
            {
                queryString.Append($" ORDER BY '{sorting.OrderBy}' {sorting.SortOrder}");
            }

            return await Task.FromResult(queryString);
        }

        private async Task<StringBuilder> AddPager(Sorting sorting, Pager pager, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy) && pager.PageNumber != 0 && pager.ItemsPerPage != 0)
            {
                queryString.Append($" OFFSET {(pager.PageNumber - 1) * pager.ItemsPerPage} ROWS FETCH NEXT {pager.ItemsPerPage} ROWS ONLY;");
            }

            return await Task.FromResult(queryString);
        }
    }
}