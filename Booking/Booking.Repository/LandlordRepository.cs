﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using Booking.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Repository
{
    public class LandlordRepository : ILandlordRepository
    {
        SqlConnection connection = new SqlConnection("Server = .; Database = Booking; Trusted_Connection = True;");
        SqlCommand cmd = null;
        StringBuilder commandString = new StringBuilder("Select * from Landlord WHERE 1=1");


        public async Task<int> GetLandlordCount()
        {
            int count = 0;
            cmd = new SqlCommand("Select count(Name) from Landlord", connection);
            connection.Open();
            count = (Int32)cmd.ExecuteScalar();
            connection.Close();
            return count;
        }

        public async Task<List<ILandlord>> GetAllLandlordsAsync(Sorting sort, Pager page, Filtering filter)
        {
            List<ILandlord> landlordList = new List<ILandlord>();
            await AddFilter(filter, commandString);
            await AddSorting(sort, commandString);
            await AddPager(sort, page, commandString);
           
            cmd = new SqlCommand(commandString.ToString(), connection); connection.Open();

            using (SqlDataReader oReader = await cmd.ExecuteReaderAsync())
            {
                while (oReader.Read())
                {
                    ILandlord landlord = new Landlord();
                    landlord.ID = int.Parse(oReader["Id"].ToString());
                    landlord.Created = Convert.ToDateTime(oReader["Created"].ToString());
                    landlord.Updated = Convert.ToDateTime(oReader["Updated"].ToString());
                    landlord.Name = oReader["Name"].ToString();
                    landlord.Surname = oReader["Surname"].ToString();
                    landlord.PhoneNumber = oReader["PhoneNumber"].ToString();
                    landlord.Email = oReader["Email"].ToString();
                    landlord.Password = oReader["Password"].ToString();
                    landlordList.Add(landlord);
                }
            }
            connection.Close();
            return landlordList;
        }

        public async Task<ILandlord> GetLandlordByIdAsync(int id)
        {
            cmd = new SqlCommand("Select * from Landlord WHERE ID=" + id, connection);
            ILandlord landlord = new Landlord();
            connection.Open();

            using (SqlDataReader oReader = await cmd.ExecuteReaderAsync())
            {
                while (oReader.Read())
                {
                    landlord.ID = int.Parse(oReader["Id"].ToString());
                    landlord.Created = Convert.ToDateTime(oReader["Created"].ToString());
                    landlord.Updated = Convert.ToDateTime(oReader["Updated"].ToString());
                    landlord.Name = oReader["Name"].ToString();
                    landlord.Surname = oReader["Surname"].ToString();
                    landlord.PhoneNumber = oReader["PhoneNumber"].ToString();
                    landlord.Email = oReader["Email"].ToString();
                    landlord.Password = oReader["Password"].ToString();
                }
            }
            connection.Close();
            return landlord;
        }

        public async Task RemoveLandlordAsync(int id)
        {
            connection.Open();
            cmd = new SqlCommand("DELETE FROM Landlord WHERE ID=" + id + ";", connection);
            await cmd.ExecuteNonQueryAsync();
            connection.Close();
        }

        public async Task SaveNewLandlordAsync(ILandlord landlord)
        {

            string commandtext = @"INSERT INTO Landlord (Created, Updated, Name, Surname, PhoneNumber, Email, Password) VALUES(@Created, @Updated, @Name, @Surname, @PhoneNumber, @Email, @Password)";
            using (connection)
            {
                cmd = new SqlCommand(commandtext, connection);
                cmd.Parameters.AddWithValue("@Created", landlord.Created);
                cmd.Parameters.AddWithValue("@Updated", landlord.Updated);
                cmd.Parameters.AddWithValue("@Name", landlord.Name);
                cmd.Parameters.AddWithValue("@Surname", landlord.Surname);
                cmd.Parameters.AddWithValue("@PhoneNumber", landlord.PhoneNumber);
                cmd.Parameters.AddWithValue("@Email", landlord.Email);
                cmd.Parameters.AddWithValue("@Password", landlord.Password);
                connection.Open();
                await cmd.ExecuteNonQueryAsync();
                connection.Close();
            }

        }
        public async Task UpdateLandlordAsync(int id, DateTime date, string phoneNumber, string email, string password)
        {
            connection.Open();
            if (phoneNumber != "")
            {
                cmd = new SqlCommand("UPDATE Landlord Set PhoneNumber='" + phoneNumber + "' WHERE ID='" + id + "'", connection);
                await cmd.ExecuteNonQueryAsync();
            }
            if (email != "")
            {
                cmd = new SqlCommand("UPDATE Landlord Set Email='" + email + "' WHERE ID='" + id + "'", connection);
                await cmd.ExecuteNonQueryAsync();
            }
            if (password != "")
            {
                cmd = new SqlCommand("UPDATE Landlord Set password='" + password + "' WHERE ID='" + id + "'", connection);
                await cmd.ExecuteNonQueryAsync();
            }

            SqlCommand cmd2 = new SqlCommand("UPDATE Landlord SET Updated='" + date + "'  WHERE ID='" + id + "'", connection);
            await cmd2.ExecuteNonQueryAsync();
            connection.Close();
        }
        private async Task<StringBuilder> AddFilter(Filtering filter, StringBuilder commandString)
        {

            if (!string.IsNullOrWhiteSpace(filter.Name))
            {
                commandString.Append($" AND (Name LIKE '%{filter.Name}%' OR Surname LIKE '%{filter.Name}%')");
            }
            if (!string.IsNullOrWhiteSpace(filter.Email))
            {
                commandString.Append($" AND Email = '{ filter.Email}' ");
            }
            return await Task.FromResult(commandString);
        }
        private async Task<StringBuilder> AddSorting(Sorting sorting, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy))
            {
                queryString.Append($" ORDER BY '{sorting.OrderBy}' {sorting.SortOrder}");
            }
            return await Task.FromResult(queryString);
        }
        private async Task<StringBuilder> AddPager(Sorting sorting, Pager pager, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy) && pager.PageNumber != 0 && pager.ItemsPerPage != 0)
            {
                queryString.Append($" OFFSET {(pager.PageNumber - 1) * pager.ItemsPerPage} ROWS FETCH NEXT {pager.ItemsPerPage} ROWS ONLY;");
            }
            return await Task.FromResult(queryString);
        }
    }
}
