﻿using Booking.Common;
using Booking.Model.Common;
using Booking.Model;
using Booking.Service.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;
using Microsoft.Owin;
using Booking.WebApi.Model;
using AutoMapper;

[assembly: OwinStartup(typeof(Booking.WebApi.App_Start.Startup))]
namespace Booking.WebApi.Controllers
{
    public class LandlordController : ApiController
    {
        private readonly IMapper mapper;
        protected ILandlordService Landlordservice { get; private set; } 
        public LandlordController(ILandlordService service, IMapper imapper) { Landlordservice = service; mapper = imapper; }
        Sorting sort = new Sorting();
        Pager page = new Pager();
        Filtering filter = new Filtering();

        // [Authorize]

        [HttpGet]
        public async Task<int> GetLandlordCount(int i)
        {
            int count = await Landlordservice.GetLandlordCount();

            return count;
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllLandlordsAsync(string sortBy = "", string orderBy = "", int itemsPerPage =0, int pageNumber = 0, string Name = "", string Surname = "", string PhoneNumber = "", string Email = "")
        {
            
            
            if (sortBy != "" || orderBy != "")
            {
                sort = new Sorting();
                sort.OrderBy = orderBy;
                sort.SortOrder = sortBy;
            }
            
           
            if (pageNumber != 0 || itemsPerPage != 0)
            {
                page = new Pager();
                page.ItemsPerPage = itemsPerPage;
                page.PageNumber = pageNumber;
            }


            if (Name != "" || Surname != "" || PhoneNumber != "" || Email != "")
            {

                filter.Name = Name;
                filter.Surname = Surname;
                filter.PhoneNumber = PhoneNumber;
                filter.Email = Email;
            }
            List<ILandlord> landlordsList = await Landlordservice.GetAllLandlordsAsync(sort, page, filter);
            if (landlordsList != null)
            {
                List<LandlordViewModel> landlordViewModelList = new List<LandlordViewModel>();
                foreach (ILandlord landlord in landlordsList)
                {
                    var landlordViewModel = mapper.Map<LandlordViewModel>(landlord);
                    landlordViewModelList.Add(landlordViewModel);
                }
                return Request.CreateResponse(HttpStatusCode.OK, landlordViewModelList);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Landlords found");
            }
        }

        [HttpGet]

        public async Task<HttpResponseMessage> GetLandlordByIdAsync(int id)
        {
            ILandlord landlord = await Landlordservice.GetLandlordByIdAsync(id);

            if (landlord.Name != null)
            {
                var landlordViewModel = mapper.Map<LandlordViewModel>(landlord);
                return Request.CreateResponse(HttpStatusCode.OK, landlordViewModel);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Landlord with id=" + id);
            }
        }

        [HttpPut]
        public async Task<HttpResponseMessage> UpdateLandlordAsync(int id, string phoneNumber = "", string email = "", string password = "")
        {
            ILandlord landlord = await Landlordservice.GetLandlordByIdAsync(id);
            if (landlord.Name == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Landlord with id=" + id);
            }
            await Landlordservice.UpdateLandlordAsync(id, phoneNumber, email, password);
            return Request.CreateResponse(HttpStatusCode.OK, "Landlord updated");
        }
    

        [HttpDelete]
        public async Task<HttpResponseMessage> RemoveLandlordAsync(int id)
        {
            ILandlord landlord = await Landlordservice.GetLandlordByIdAsync(id);
            if (landlord.Name == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Landlord with id=" + id);
            }

            await Landlordservice.RemoveLandlordAsync(id);

            return Request.CreateResponse(HttpStatusCode.OK, "Landlord removed");
        }

        [HttpPost]
        public async Task<HttpResponseMessage> SaveNewLandlordAsync([FromBody] LandlordViewModel landlordView)
        {
            List<ILandlord> listOfLandlords = await Landlordservice.GetAllLandlordsAsync(sort, page, filter);

            ILandlord email = listOfLandlords.Where(s => s.Email == landlordView.Email).FirstOrDefault();
            if (email != null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "This email address is already being used");
            }
            ILandlord landlord = new Landlord();
         
            landlord=mapper.Map<ILandlord>(landlordView);
           
            await Landlordservice.SaveNewLandlordAsync(landlord);
            return Request.CreateResponse(HttpStatusCode.OK, "Landlord added");
        }

    }
}
