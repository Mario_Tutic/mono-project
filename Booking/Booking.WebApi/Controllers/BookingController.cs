﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using Booking.Service.Common;
using Booking.WebApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Booking.WebApi.Controllers
{
    public class BookingController : ApiController
    {
        public BookingController(IBookingService Service)
        {
            this.Service = Service;
        }

        public Sorting sort = new Sorting();
        public Pager page = new Pager();
        public Filtering filter = new Filtering();

        protected IBookingService Service { get; set; }
        public async Task<HttpResponseMessage> GetAllBookingsAsync(string sortBy = "ID", string orderBy = "", int itemsPerPage = 3, int pageNumber = 1, int ApartmentID = 0, int TenantID = 0, DateTime? BookInDate = null, DateTime? BookOutDate = null)
        {
            if (sortBy != "" || orderBy != "")
            {
                sort = new Sorting();
                sort.OrderBy = orderBy;
                sort.SortOrder = sortBy;
            }


            if (pageNumber != 0 || itemsPerPage != 0)
            {
                page = new Pager();
                page.ItemsPerPage = itemsPerPage;
                page.PageNumber = pageNumber;
            }

            if (ApartmentID !=0 || TenantID != 0 || BookInDate != null || BookOutDate != null)
            {
                filter = new Filtering();
                filter.ApartmentID = ApartmentID;
                filter.TenantID = TenantID;
                if(BookInDate != null)
                filter.BookInDate = (DateTime)BookInDate;
                if(BookOutDate != null)
                filter.BookInDate = (DateTime)BookOutDate;
            }

            List<IOnlineBooking> bookings=new List<IOnlineBooking>();

            bookings = await Service.GetAllBookingsAsync(sort, page, filter);
            return Request.CreateResponse(HttpStatusCode.OK, bookings);
        }
        public async Task<HttpResponseMessage> GetBookingByIdAsync(int id)
        {
            IOnlineBooking booking;
            booking = await Service.GetBookingByIdAsync(id);
            if (booking != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, booking);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Booking is found with that id");
            }
        }
        public async Task<HttpResponseMessage> DeleteBookingAsync(int id)
        {
            IOnlineBooking booking;
            booking = await Service.GetBookingByIdAsync(id);
            if (booking != null)
            {
                await Service.DeleteBookingAsync(id);
                return Request.CreateResponse(HttpStatusCode.OK,"Booking is deleted");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Booking is found with that id");
            }
        }
        public async Task<HttpResponseMessage> PostNewBookingAsync(OnlineBookingViewModel TempNewBooking)
        {
            OnlineBooking NewBooking = new OnlineBooking();
            NewBooking.ApartmentID = TempNewBooking.ApartmentID;
            NewBooking.TenantID = TempNewBooking.TenantID;
            NewBooking.BookInDate = TempNewBooking.BookInDate;
            NewBooking.BookOutDate = TempNewBooking.BookOutDate;
            await Service.PostNewBookingAsync(NewBooking);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        public async Task<HttpResponseMessage> PutNewBookingAsync(OnlineBookingViewModel TempNewBooking,int id)
        {
            IOnlineBooking booking;
            booking = await Service.GetBookingByIdAsync(id);
            if (booking != null)
            {
                OnlineBooking NewBooking = new OnlineBooking();
                NewBooking.ApartmentID = TempNewBooking.ApartmentID;
                NewBooking.TenantID = TempNewBooking.TenantID;
                NewBooking.BookInDate = TempNewBooking.BookInDate;
                NewBooking.BookOutDate = TempNewBooking.BookOutDate;
                await Service.PutBookingAsync(NewBooking, id);
                return Request.CreateResponse(HttpStatusCode.OK,"Booking is updated");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Booking is found with that id");
            }
        }

    }
}
