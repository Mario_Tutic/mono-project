﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Booking.Model.Common;
using Booking.Model;
using Booking.Service.Common;
using Booking.Common;
using Booking.WebApi.Model;
using AutoMapper;

using System.Linq;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(Booking.WebApi.App_Start.Startup))]
namespace Booking.WebApi.Controllers
{
    public class TenantController :  ApiController
    {
        private readonly IMapper mapper;

        protected ITenantService Tenantservice { get; private set; }
        public TenantController(ITenantService service, IMapper imapper) {  Tenantservice=service; mapper = imapper; }
        public Sorting sort = new Sorting();
        public Pager page = new Pager();
        public Filtering filter = new Filtering();
        
      //  [Authorize]


        [HttpGet]
        public async Task<int> GetTenantCount(int i)
        {
            int count = await Tenantservice.GetTenantCount();
            
            return count; 
        }


        [HttpGet]
        public async Task<HttpResponseMessage> GetAllTenantsAsync(string sortBy="", string orderBy="", int itemsPerPage=0, int pageNumber=0, string Name="", string Surname="",string PhoneNumber="",string Email="")
        {   

           
            if (sortBy != "" || orderBy != "")
            {
                sort = new Sorting();
                sort.OrderBy = orderBy;
                sort.SortOrder = sortBy;
            }

           
            if (pageNumber != 0 || itemsPerPage!= 0)
            {
                page = new Pager();
                page.ItemsPerPage = itemsPerPage;
                page.PageNumber = pageNumber;
            }

            if (Name != "" || Surname != "" || Email!="") 
            {
                
                filter.Name = Name;
                filter.Surname = Surname;
                filter.Email = Email;
            }

            List<ITenant> tenantsList = await Tenantservice.GetAllTenantsAsync(sort, page, filter);
            if (tenantsList != null)
            {
                List<TenantViewModel> tenantViewModelList = new List<TenantViewModel>();
                foreach(ITenant tenant in tenantsList)
                {
                    var tenantViewModel = mapper.Map<TenantViewModel>(tenant);
                    tenantViewModelList.Add(tenantViewModel);
                }

                return Request.CreateResponse(HttpStatusCode.OK, tenantViewModelList);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound,"No Tenants found");
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetTenantByIdAsync(int id)
        {
            ITenant tenant = await Tenantservice.GetTenantByIdAsync(id);
            if (tenant.Name != null)
            {
                var tenantViewModel = mapper.Map<TenantViewModel>(tenant);
                return Request.CreateResponse(HttpStatusCode.OK, tenantViewModel);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Tenant with id="+id);
            }
        }

        [HttpPut]
        public async Task<HttpResponseMessage> UpdateTenantAsync(int id, string phoneNumber = "", string email = "", string password = "")
        {
            ITenant tenant = await Tenantservice.GetTenantByIdAsync(id);
            if (tenant.Name == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Tenant with id=" + id);
            }
            await Tenantservice.UpdateTenantAsync(id, phoneNumber, email, password);
            return Request.CreateResponse(HttpStatusCode.OK, "Tenant updated");
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> RemoveTenantAsync(int id)
        {
            ITenant tenant = await Tenantservice.GetTenantByIdAsync(id);
            if (tenant.Name == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Tenant with id=" + id);
            }
            await Tenantservice.RemoveTenantAsync(id);

            return Request.CreateResponse(HttpStatusCode.OK, "Tenant removed");
        }

        [HttpPost]
        public async Task<HttpResponseMessage> SaveNewTenantAsync([FromBody] TenantViewModel tenantView)
        {

            List<ITenant> listOfTenants=await Tenantservice.GetAllTenantsAsync(sort,page,filter);

            ITenant email=listOfTenants.Where(s=>s.Email==tenantView.Email).FirstOrDefault();
            if (email != null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "This email address is already being used");
            }
            ITenant newTenant = new Tenant();
            newTenant.Created = DateTime.UtcNow;
            newTenant.Updated= DateTime.UtcNow;
            newTenant = mapper.Map<TenantViewModel,ITenant>(tenantView);


            await Tenantservice.SaveNewTenantAsync(newTenant);
            return Request.CreateResponse(HttpStatusCode.OK, "Tenant added");
        }     
    }
}
