﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Model.Common
{
    public interface ITenant
    {
        int ID { set; get; }
        DateTime Created { set; get; }
        DateTime Updated { set; get; }
        string Name { set; get; }
        string Surname { set; get; }
        string PhoneNumber { set; get; }
        string Email { set; get; }
        string Password { set; get; }
    }
}
