﻿using Booking.Model;
using Booking.Model.Common;
using Booking.Service.Common;
using Booking.WebApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace Booking.WebApi.Controllers
{


    public class ReviewController : ApiController
    {
        public ReviewController(IReviewService Service)
        {
            this.Service = Service;
        }
        protected IReviewService Service { get; set; }



        public async Task<HttpResponseMessage> GetAllReviewsAsync()
        {
            List<IReview> reviews = new List<IReview>();
            reviews = await Service.GetAllReviewsAsync();
            if (reviews != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, reviews);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Reviews found");
            }
        }


        public async Task<HttpResponseMessage> GetReviewByIdAsync(int id)
        {
            Review review = new Review();
            review = (Review)await Service.GetReviewByIdAsync(id);
            if (review != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, review);
            }
            else 
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Review found");
            }
            
        }

        public async Task<HttpResponseMessage> DeleteReviewAsync(int id)
        {
            Review review = new Review();
            review = (Review)await Service.GetReviewByIdAsync(id);
            if (review != null)
            {
                await Service.DeleteReviewAsync(id);
                return Request.CreateResponse(HttpStatusCode.OK, "Review is deleted");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Review with that id found");
            }
        }
        public async Task<HttpResponseMessage> PostNewReviewAsync(ReviewViewModel TempNewReview)
        {
            Review NewReview = new Review();
            NewReview.ApartmentID = TempNewReview.ApartmentID;
            NewReview.TenantID = TempNewReview.TenantID;
            NewReview.ReviewText = TempNewReview.ReviewText;
            NewReview.ID = 0;
            NewReview.Updated = DateTime.Now;
            NewReview.Created= DateTime.Now;

            await Service.PostNewReviewAsync(NewReview);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        public async Task<HttpResponseMessage> PutReviewAsync(string NewReviewText, int id)
        {
            Review review = new Review();
            review = (Review)await Service.GetReviewByIdAsync(id);
            if (review != null)
            {
                await Service.PutReviewAsync(NewReviewText, id);
                return Request.CreateResponse(HttpStatusCode.OK,"Review is altered");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "No Review with that id found");
            }
        }
    }
}
