﻿using Booking.Common;
using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Repository.Common
{
    public interface IApartmentRepository
    {
        Task<List<IApartment>> GetAllApartmentsAsync(Sorting sort, Pager page, Filtering filter);

        Task<IApartment> GetApartmentByIdAsync(int id);

        Task SaveNewApartmentAsync(IApartment apartment);

        Task UpdateApartmentAsync(int id, DateTime date, double rating, int price, int rooms, bool free);

        Task RemoveApartmentAsync(int id);
    }
}
