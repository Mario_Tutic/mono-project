﻿using Booking.Common;
using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Repository.Common
{
    public interface ILandlordRepository
    {
        Task<List<ILandlord>> GetAllLandlordsAsync(Sorting sort, Pager page, Filtering filter);
        Task<ILandlord> GetLandlordByIdAsync(int id);

        Task SaveNewLandlordAsync(ILandlord address);

        Task UpdateLandlordAsync(int id, DateTime date, string phoneNumber, string email, string password);

        Task RemoveLandlordAsync(int id);
        Task<int> GetLandlordCount();
    }
}
