﻿using Booking.Common;
using Booking.Model.Common;
using Booking.Repository;
using Booking.Repository.Common;
using Booking.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Service
{
    public class AddressService : IAddressService
    {
        protected IAddressRepository repository { get; private set; }

        public AddressService(IAddressRepository _repository)
        {
            repository = _repository;
        }

        public async Task<List<IAddress>> GetAllAddressesAsync(Sorting sort, Pager page, Filtering filter)
        {
            return await repository.GetAllAddressesAsync(sort, page, filter);
        }

        public async Task<IAddress> GetAddressByIdAsync(int id)
        {
            return await repository.GetAddressByIdAsync(id);
        }

        public async Task SaveNewAddressAsync(IAddress address)
        {
            address.Created = DateTime.UtcNow;
            address.Updated = DateTime.UtcNow;

            await repository.SaveNewAddressAsync(address);
        }

        public async Task UpdateAddressAsync(int id, string country, string city, string street, int house, int flat)
        {
            DateTime date = DateTime.UtcNow;

            await repository.UpdateAddressAsync(id, date, country, city, street, house, flat);
        }

        public async Task RemoveAddressAsync(int id)
        {
            await repository.RemoveAddressAsync(id);
        }
    }
}