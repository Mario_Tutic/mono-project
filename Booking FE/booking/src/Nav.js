import React  from "react";
import './App.css';
import {Link} from 'react-router-dom';

function Nav(){
    const navStyle={
        color: 'rgb(73, 72, 72)',
        textDecoration: 'none'
    };



    return(
        <nav >
            <Link style={navStyle} to='/'>
            <h3>Booking</h3>  
            </Link>
            <ul className="navLinks">
                <Link style={navStyle} to='/tenants'>
                <li>Tenants</li>
                </Link>
                <Link style={navStyle} to='/landlords'>
                <li>Landlords </li>
                </Link>
                
            </ul>
        </nav>
    )
}

export default Nav;