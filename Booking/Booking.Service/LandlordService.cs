﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Booking.Common;
using Booking.Model.Common;
using Booking.Repository.Common;
using Booking.Service.Common;
namespace Booking.Service
{
    public class LandlordService : ILandlordService
    {
        protected ILandlordRepository LandlordRepository { get; private set; }
        public LandlordService(ILandlordRepository repository) { LandlordRepository = repository; }
        public async Task<int> GetLandlordCount()
        {
            return await LandlordRepository.GetLandlordCount();
        }

        public async Task<List<ILandlord>> GetAllLandlordsAsync(Sorting sort, Pager page, Filtering filter)
        {
            return await LandlordRepository.GetAllLandlordsAsync(sort, page, filter);
        }
        public async Task<ILandlord> GetLandlordByIdAsync(int id)
        {
            return await LandlordRepository.GetLandlordByIdAsync(id);
        }
        public async Task RemoveLandlordAsync(int id)
        {
            await LandlordRepository.RemoveLandlordAsync(id);
        }
        public async Task SaveNewLandlordAsync(ILandlord landlord)
        {
            DateTime date = DateTime.UtcNow;
            landlord.Created = DateTime.UtcNow;
            landlord.Updated = DateTime.UtcNow;
            await LandlordRepository.SaveNewLandlordAsync(landlord);
        }
        public async Task UpdateLandlordAsync(int id, string phoneNumber, string email, string password)
        {
            DateTime date = DateTime.UtcNow;
            await LandlordRepository.UpdateLandlordAsync(id, date, phoneNumber, email, password);
        }
    }
}
