﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Booking.Model.Common;
using Booking.Repository.Common;
using System.Data.SqlClient;
using Booking.Model;
using Booking.Common;

namespace Booking.Repository
{
    public class TenantRepository : ITenantRepository
    {
        SqlConnection connection = new SqlConnection("Server = .; Database = Booking; Trusted_Connection = True;");
        SqlCommand cmd = null;
        StringBuilder commandString = new StringBuilder("Select * from Tenant WHERE 1=1");

        public async Task<int> GetTenantCount()
        {
            int count = 0;
            cmd = new SqlCommand("Select count(Name) from tenant", connection);
            connection.Open();
            count = (Int32) cmd.ExecuteScalar();
            connection.Close();
                return count;
            }
        
        public async Task<List<ITenant>> GetAllTenantsAsync(Sorting sort, Pager page, Filtering filter)
        {
            List<ITenant> tenantList = new List<ITenant>();
            await AddFilter(filter, commandString);
            await AddSorting(sort, commandString);
            await AddPager(sort, page, commandString);
            cmd = new SqlCommand(commandString.ToString(), connection);
            connection.Open();

            using (SqlDataReader oReader = await cmd.ExecuteReaderAsync())
            {
                while (oReader.Read())
                {
                    ITenant tenant = new Tenant();
                    tenant.ID = int.Parse(oReader["Id"].ToString());
                    tenant.Created = Convert.ToDateTime(oReader["Created"].ToString());
                    tenant.Updated = Convert.ToDateTime(oReader["Updated"].ToString());
                    tenant.Name = oReader["Name"].ToString();
                    tenant.Surname = oReader["Surname"].ToString();
                    tenant.PhoneNumber = oReader["PhoneNumber"].ToString();
                    tenant.Email=oReader["Email"].ToString();
                    tenant.Password = oReader["Password"].ToString();
                    tenantList.Add(tenant);
                }
            }
            connection.Close();
            return tenantList;
        }

        public async Task<ITenant> GetTenantByIdAsync(int id)
        {
            cmd = new SqlCommand("Select * from Tenant WHERE ID="+id, connection);

            ITenant tenant = new Tenant();

            connection.Open();

            using (SqlDataReader oReader = await cmd.ExecuteReaderAsync())
            {
                while (oReader.Read())
                {
                    tenant.ID = int.Parse(oReader["Id"].ToString());
                    tenant.Created = Convert.ToDateTime(oReader["Created"].ToString());
                    tenant.Updated = Convert.ToDateTime(oReader["Updated"].ToString());
                    tenant.Name = oReader["Name"].ToString();
                    tenant.Surname = oReader["Surname"].ToString();
                    tenant.PhoneNumber = oReader["PhoneNumber"].ToString();
                    tenant.Email = oReader["Email"].ToString();
                    tenant.Password = oReader["Password"].ToString();
                }
            }
            connection.Close();
            return tenant;
        }

        public async Task RemoveTenantAsync(int id)
        {
            connection.Open();
            cmd = new SqlCommand("DELETE FROM Tenant WHERE ID="+id+";",connection);
            await cmd.ExecuteNonQueryAsync();
            connection.Close();
        }

        public async Task SaveNewTenantAsync(ITenant tenant)
        {
            
            string commandtext = @"INSERT INTO Tenant (Created, Updated, Name, Surname, PhoneNumber, Email, Password) VALUES(@Created, @Updated, @Name, @Surname, @PhoneNumber, @Email, @Password)";
            using (connection) {
                cmd = new SqlCommand(commandtext, connection);
                cmd.Parameters.AddWithValue("@Created", tenant.Created);
                cmd.Parameters.AddWithValue("@Updated", tenant.Updated);
                cmd.Parameters.AddWithValue("@Name", tenant.Name);
                cmd.Parameters.AddWithValue("@Surname", tenant.Surname);
                cmd.Parameters.AddWithValue("@PhoneNumber", tenant.PhoneNumber);
                cmd.Parameters.AddWithValue("@Email", tenant.Email);
                cmd.Parameters.AddWithValue("@Password", tenant.Password);
                connection.Open();
                await cmd.ExecuteNonQueryAsync();
                connection.Close();
            }
        
        }


    public async Task UpdateTenantAsync(int id,DateTime date, string phoneNumber, string email, string password)
        {
            connection.Open();
            if (phoneNumber != "")
            {
                cmd = new SqlCommand("UPDATE Tenant Set PhoneNumber='" + phoneNumber + "' WHERE ID='" + id + "'", connection);
                await cmd.ExecuteNonQueryAsync();
            }
            if(email != "")
            {
                cmd = new SqlCommand("UPDATE Tenant Set Email='" + email + "' WHERE ID='" + id + "'", connection);
                await cmd.ExecuteNonQueryAsync();
            }
            if(password != "")
            {
                cmd = new SqlCommand("UPDATE Tenant Set password='" + password + "' WHERE ID='" + id + "'", connection);
                await cmd.ExecuteNonQueryAsync();
            }
            
            SqlCommand cmd2 = new SqlCommand("UPDATE Tenant SET Updated='" + date + "'  WHERE ID='" + id + "'", connection);
            await cmd2.ExecuteNonQueryAsync();
            connection.Close();
        }
        private async Task<StringBuilder> AddFilter(Filtering filter, StringBuilder commandString)
        {

            if (!string.IsNullOrWhiteSpace(filter.Name))
            {
                commandString.Append($" AND (Name LIKE '%{filter.Name}%' OR Surname LIKE '%{filter.Name}%')");
            }
            if (!string.IsNullOrWhiteSpace(filter.Email))
            {
                commandString.Append($" AND Email = '{ filter.Email}' ");
            }
            return await Task.FromResult(commandString);
        }
      private async Task<StringBuilder> AddSorting(Sorting sorting, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy))
            {
                queryString.Append($" ORDER BY '{sorting.OrderBy}' {sorting.SortOrder}");
            }
            return await Task.FromResult(queryString);
        }
        private async Task<StringBuilder> AddPager(Sorting sorting, Pager pager, StringBuilder queryString)
        {
            if (!string.IsNullOrWhiteSpace(sorting.OrderBy) && pager.PageNumber != 0 && pager.ItemsPerPage != 0)
            {
                queryString.Append($" OFFSET {(pager.PageNumber - 1) * pager.ItemsPerPage} ROWS FETCH NEXT {pager.ItemsPerPage} ROWS ONLY;");
            }
            return await Task.FromResult(queryString);
        }
    }
    }

