﻿using Booking.Model;
using Booking.Model.Common;
using Booking.Repository.Common;
using Booking.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Service
{
    class ReviewService : IReviewService
    {

        public ReviewService(IReviewRepository Repository)
        {
            this.Repository = Repository;
        }
        protected IReviewRepository Repository { get; set; }
        public async Task<List<IReview>> GetAllReviewsAsync()
        {
            return await Repository.GetAllReviewsAsync();
        }

        public async Task<IReview> GetReviewByIdAsync(int id)
        {
            return await Repository.GetReviewByIdAsync(id);
        }

        public async Task DeleteReviewAsync(int id)
        {
            await Repository.DeleteReviewAsync(id);
            return;
        }

        public async Task PostNewReviewAsync(IReview NewReview)
        {
            await Repository.PostNewReviewAsync(NewReview);
        }

        public async Task PutReviewAsync(string NewReviewText, int id)
        {
            await Repository.PutReviewAsync(NewReviewText,id);
        }
    }
}
