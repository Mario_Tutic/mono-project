﻿using Booking.Common;
using Booking.Repository;
using Booking.Repository.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin.Security.OAuth;
namespace Booking.WebApi.Providers
{
    public class OauthProvider : OAuthAuthorizationServerProvider
    {
        public OauthProvider()
        {
        }
        protected ILandlordRepository LandlordRepository = new LandlordRepository();
        protected ITenantRepository tenantRepository = new TenantRepository();
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            await Task.Run(() => context.Validated());
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            Sorting sort = new Sorting();
            Filtering filter = new Filtering();
            Pager page = new Pager();
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            var tenants = await tenantRepository.GetAllTenantsAsync(sort,page, filter);
            var landlord = await LandlordRepository.GetAllLandlordsAsync(sort,page, filter);    
            if (tenants != null || landlord != null  )
            {
                var user = tenants.Where(o => o.Email == context.UserName && o.Password == context.Password).FirstOrDefault();
                var    user2 = landlord.Where(o => o.Email == context.UserName && o.Password == context.Password).FirstOrDefault();
                if (user != null)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Name, user.Name));
                    identity.AddClaim(new Claim("LoggedOn", DateTime.Now.ToString()));
                    await Task.Run(() => context.Validated(identity));
                    return;
                }
                if (user2 != null)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Name, user2.Name));
                    identity.AddClaim(new Claim("LoggedOn", DateTime.Now.ToString()));
                    await Task.Run(() => context.Validated(identity));
                }
                else
                {
                    context.SetError("Wrong Credentials", "Provided username and password is incorrect");
                }
            }
            else
            {
                context.SetError("Wrong Credentials", "Provided username and password is incorrect");
            }
            return;
        }
    }
}