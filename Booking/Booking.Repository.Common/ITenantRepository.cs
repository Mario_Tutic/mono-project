﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Repository.Common
{
    public interface ITenantRepository
    {
        Task<List<ITenant>> GetAllTenantsAsync(Sorting sort, Pager page, Filtering filter);
        Task<ITenant> GetTenantByIdAsync(int id);

        Task SaveNewTenantAsync(ITenant tenant);

        Task UpdateTenantAsync(int id,DateTime date, string phoneNumber, string email, string password);

        Task RemoveTenantAsync(int id);

        Task<int> GetTenantCount();
    }
}
