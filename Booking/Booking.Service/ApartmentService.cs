﻿using Booking.Common;
using Booking.Model.Common;
using Booking.Repository;
using Booking.Repository.Common;
using Booking.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Service
{
    public class ApartmentService : IApartmentService
    {
        protected IApartmentRepository apartmentRepository { get; private set; }
        protected IAddressRepository addressRepository { get; private set; }
        protected ILandlordRepository landlordRepository { get; private set; }

        public ApartmentService(IApartmentRepository _apartmentRepository, IAddressRepository _addressRepository, ILandlordRepository _landlordRepository)
        {
            apartmentRepository = _apartmentRepository;
            addressRepository = _addressRepository;
            landlordRepository = _landlordRepository;
        }

        public async Task<List<IApartment>> GetAllApartmentsAsync(Sorting sort, Pager page, Filtering filter)
        {
            List<ILandlord> landlordList = await landlordRepository.GetAllLandlordsAsync(sort, page, filter);
            List<IApartment> apartmentList = await apartmentRepository.GetAllApartmentsAsync(sort, page, filter);
            List<IApartment> apartmentWithAdressAndLandlordList = new List<IApartment>();

            foreach (IApartment apartment in apartmentList)
            {
                apartment.Address = await addressRepository.GetAddressByIdAsync(apartment.ID);
                apartment.Landlord = await landlordRepository.GetLandlordByIdAsync(apartment.ID);
                apartmentWithAdressAndLandlordList.Add(apartment);
            }

            return apartmentWithAdressAndLandlordList;
        }

        public async Task<IApartment> GetApartmentByIdAsync(int id)
        {
            IApartment apartment = await apartmentRepository.GetApartmentByIdAsync(id);
            apartment.Address = await addressRepository.GetAddressByIdAsync(apartment.ID);
            apartment.Landlord = await landlordRepository.GetLandlordByIdAsync(apartment.ID);

            return apartment;
        }

        public async Task SaveNewApartmentAsync(IApartment apartment)
        {
            apartment.Created = DateTime.UtcNow;
            apartment.Updated = DateTime.UtcNow;
            apartment.Address = await addressRepository.GetAddressByIdAsync(apartment.ID);

            await apartmentRepository.SaveNewApartmentAsync(apartment);
        }

        public async Task UpdateApartmentAsync(int id, double rating, int price, int rooms, bool free)
        {
            DateTime date = DateTime.UtcNow;
            IApartment apartment = await apartmentRepository.GetApartmentByIdAsync(id);
            apartment.Address = await addressRepository.GetAddressByIdAsync(apartment.ID);

            await apartmentRepository.UpdateApartmentAsync(id, date, rating, price, rooms, free);
        }

        public async Task RemoveApartmentAsync(int id)
        {
            IApartment apartment = await apartmentRepository.GetApartmentByIdAsync(id);
            apartment.Address = await addressRepository.GetAddressByIdAsync(apartment.ID);

            await apartmentRepository.RemoveApartmentAsync(id);
        }
    }
}