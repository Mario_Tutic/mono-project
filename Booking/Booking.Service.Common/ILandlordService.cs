﻿using Booking.Common;
using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Service.Common
{
    public interface ILandlordService
    {
        Task<List<ILandlord>> GetAllLandlordsAsync(Sorting sort, Pager page, Filtering filter);
        Task<ILandlord> GetLandlordByIdAsync(int id);
        Task RemoveLandlordAsync(int id);
        Task SaveNewLandlordAsync(ILandlord landlord);
        Task UpdateLandlordAsync(int id, string phoneNumber, string email, string password);
        Task<int> GetLandlordCount();
    }
}
