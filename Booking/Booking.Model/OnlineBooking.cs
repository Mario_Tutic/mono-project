﻿using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Model
{
    public class OnlineBooking : IOnlineBooking
    {
        public int ID { set; get; }
        public DateTime Created { set; get; }
        public DateTime Updated { set; get; }
        public DateTime BookInDate { set; get; }
        public DateTime BookOutDate { set; get; }
        public int ApartmentID { set; get; }
        public int TenantID { set; get; }
    }
}
