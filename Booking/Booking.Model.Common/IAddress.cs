﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Model.Common
{
    public interface IAddress
    {
        int ID { set; get; }
        DateTime Created { set; get; }
        DateTime Updated { set; get; }
        string Country { set; get; }
        string City { set; get; }
        string Street { set; get; }
        int House { set; get; }
        int Flat { set; get; }
    }
}
