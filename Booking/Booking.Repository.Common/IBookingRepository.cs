﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Repository.Common
{
    public interface IBookingRepository
    {
        Task<List<IOnlineBooking>> GetAllBookingsAsync(Sorting sort, Pager page, Filtering filter);
        Task<IOnlineBooking> GetBookingByIdAsync(int id);
        Task DeleteBookingAsync(int id);
        Task PostNewBookingAsync(IOnlineBooking NewBooking);
        Task PutBookingAsync(IOnlineBooking NewBooking, int ID);
    }
}
