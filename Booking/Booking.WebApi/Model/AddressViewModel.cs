﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking.WebApi.Model
{
    public class AddressViewModel
    {
        public DateTime Created { set; get; }
        public DateTime Updated { set; get; }
        public string Country { set; get; }
        public string City { set; get; }
        public string Street { set; get; }
        public int House { set; get; }
        public int Flat { set; get; }
    }
}