using Booking.Repository.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;


namespace Booking.Repository
{
    public class RepositoryDIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LandlordRepository>()
                       .As<ILandlordRepository>()
                       .InstancePerRequest();

            builder.RegisterType<TenantRepository>()
                       .As<ITenantRepository>()
                       .InstancePerRequest();

            builder.RegisterType<AddressRepository>()
                       .As<IAddressRepository>()
                       .InstancePerRequest();

            builder.RegisterType<BookingRepository>()
                       .As<IBookingRepository>()
                       .InstancePerRequest();

            builder.RegisterType<ApartmentRepository>()
                       .As<IApartmentRepository>()
                       .InstancePerRequest();

            builder.RegisterType<ReviewRepository>()
                       .As<IReviewRepository>()
                        .InstancePerRequest();
        }
    }
}