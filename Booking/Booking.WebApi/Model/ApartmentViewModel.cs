﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking.WebApi.Model
{
    public class ApartmentViewModel
    {
        public DateTime Created { set; get; }
        public DateTime Updated { set; get; }
        public LandlordPersonalViewModel Landlord { set; get; }
        public AddressViewModel Address { get; set; }
        public double Rating { set; get; }
        public int Price { set; get; }
        public int Rooms { set; get; }
        public bool Free { set; get; }
    }
}