import React, { Component } from 'react'
import ReviewService from '../services/ReviewService';

class UpdateReviewComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            ReviewText: '',
            TenantID: '',
            ApartmentID: ''
        }
        this.changeReviewTextHandler = this.changeReviewTextHandler.bind(this);
    }

    componentDidMount(){
        ReviewService.getReviewById(this.state.id).then( (res) =>{
            let TempNewReview = res.data;
            this.setState({ReviewText: TempNewReview.ReviewText,
                    TenantID: TempNewReview.TenantID,
                    ApartmentID: TempNewReview.ApartmentID
            });
        });
    }

    updateReview = (e) => {
        e.preventDefault();
        console.log('NewReviewText => ' + JSON.stringify(this.state.ReviewText));
        console.log('id => ' + JSON.stringify(this.state.id));
        ReviewService.updateReview(this.state.ReviewText, this.state.id).then( res => {
            this.props.history.push('/reviews');
        });
    }
    
    changeReviewTextHandler= (event) => {
        this.setState({ReviewText: event.target.value});
    }

    cancel(){
        this.props.history.push('/reviews');
    }

    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                <h3 className="text-center">Update Review</h3>
                                <div className = "card-body">
                                    <form>
                                    <div className = "form-group">
                                            <label> ReviewText: </label>
                                            <input placeholder="Review Text" name="reviewText" className="form-control" 
                                            value={this.state.ReviewText} onChange={this.changeReviewTextHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.updateReview}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default UpdateReviewComponent


