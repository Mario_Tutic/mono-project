import axios from 'axios';

const BOOKING_API_URL = "https://localhost:44315/api/booking";

class BookingService {

    getBookings(){
        return axios.get(BOOKING_API_URL);
    }

    createBooking(booking){
        return axios.post(BOOKING_API_URL, booking);
    }

    getBookingById(bookingId){
        return axios.get(BOOKING_API_URL + '/' + bookingId);
    }

    updateBooking(booking, bookingId){
        return axios.put(BOOKING_API_URL + '/' + bookingId, booking);
    }

    deleteBooking(bookingId){
        return axios.delete(BOOKING_API_URL + '/' + bookingId);
    }
}

export default new BookingService()