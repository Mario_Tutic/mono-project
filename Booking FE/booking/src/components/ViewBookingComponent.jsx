import React, { Component } from 'react'
import BookingService from '../services/BookingService'

class ViewBookingComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            booking: {}
        }
    }

    componentDidMount(){
        BookingService.getBookingById(this.state.id).then( res => {
            this.setState({booking: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Booking Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Booking Id: </label>
                            <div> { this.state.booking.ID }</div>
                        </div>
                        <div className = "row">
                            <label> Book In Date: </label>
                            <div> { this.state.booking.BookInDate }</div>
                        </div>
                        <div className = "row">
                            <label> Book Out Date: </label>
                            <div> { this.state.booking.BookOutDate }</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ViewBookingComponent
