import React, {useState, useEffect} from "react";

import './tenantDetail.css';


function Landlord({match}){

    useEffect(() => {
       fetchLandlord();
       console.log(match);
    },[]);

    const [landlord, setLandlord]=useState({});

    const fetchLandlord = async () => {
        const fetchLandlord = await fetch('https://localhost:44315/api/Landlord/?Id='+match.params.id);
        const landlord = await fetchLandlord.json();
        setLandlord(landlord)
        console.log(landlord);
    };



        return(
            <div class="wrapper">
            <div class="left">
                <img src="https://www.machinecurve.com/wp-content/uploads/2019/07/thispersondoesnotexist-1.jpg" alt="user" width="100"/>
               
                <h4>{landlord.Name} {landlord.Surname}</h4>
                
            </div>
            <div class="right">
                <div class="info">
                    <h3>Information</h3>
                    <div class="info_data">
                         <div class="data">
                            <h4>Email</h4>
                            <p>{landlord.Email}</p>
                         </div>
                         <div class="data">
                           <h4>Phone</h4>
                            <p>{landlord.PhoneNumber}</p>
                      </div>
                    </div>
                </div>
              
              <div class="projects">
                    <h3>Bookings</h3>
                    <div class="projects_data">
                         <div class="data">
                            <h4>Recent bookings</h4>
                            <p>Lorem ipsum dolor sit amet.</p>
                         </div>
                         <div class="data">
                           <h4>Most Visited</h4>
                            <p>dolor sit amet.</p>
                      </div>
                    </div>
                </div>
              
                
            </div>
        </div>
                )
            }
        
 


export default Landlord;