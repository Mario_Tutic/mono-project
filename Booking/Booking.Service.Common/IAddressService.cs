﻿using Booking.Common;
using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Service.Common
{
    public interface IAddressService
    {
        Task<List<IAddress>> GetAllAddressesAsync(Sorting sort, Pager page, Filtering filter);

        Task<IAddress> GetAddressByIdAsync(int id);

        Task SaveNewAddressAsync(IAddress address);

        Task UpdateAddressAsync(int id, string country, string city, string street, int house, int flat);

        Task RemoveAddressAsync(int id);
    }
}
