import React, {useState, useEffect} from "react";
import ReactDOM from "react-dom";
import {Table, Button} from "reactstrap";
import 'react-dropdown/style.css';
import {Link} from "react-router-dom";
import axios from 'axios';
import Pagination from '@material-ui/lab/Pagination';

function Landlords(){


    const [landlords, setLandlords]=useState([]);
    const[itemsPerPage, setItemsPerPage]=useState(3);
    const[pageNumber,setPageNumber]=useState(1);
    const[landlordsCount,setCount]=useState(0);



    useEffect(() => {
        fetchLandlords();
        getLandlordsCount();
    },[itemsPerPage,pageNumber]);


    const getLandlordsCount=async()=>{
        let data= await fetch('https://localhost:44315/api/Landlord/?i=1');
        let datajson = await data.json();
        setCount(datajson);
        console.log(landlordsCount)
    }

    const fetchLandlords = async() =>{
    const data= await fetch('https://localhost:44315/api/Landlord/?sortBy=ASC&orderBy=Id&itemsPerPage='+itemsPerPage+'&pageNumber='+pageNumber);
    const landlordsjson = await data.json();
    setLandlords(landlordsjson); };
    console.log(Landlords);

    const navStyle={
        color: 'rgb(73, 72, 72)',
        textDecoration: 'none'
    };

    function deleteLandlord(id){
        axios.delete('https://localhost:44315/api/Landlord/'+id).then(()=>{
            fetchLandlords();
        })
    }

    function handleChange(event,value){   
        setPageNumber(value)
        console.log(value)
      }
      
     
     
      
      function setItemsPerPageChangeHandle(event){
        setItemsPerPage(parseInt(event.target.value))
      }
     
      function setNumberOfPages(){
        if (landlordsCount%itemsPerPage) return parseInt(landlordsCount/itemsPerPage)+1
        else return parseInt(landlordsCount/itemsPerPage)
    }

    let landlordsList = landlords.map((user) => {
        return (
          <tr  key={user.Id}>
            <td>{user.Id}</td>
            <td ><Link style={navStyle} to={`/landlords/${user.Id}`}>{user.Name} </Link></td>
           
            
           <td><Link style={navStyle} to={`/landlords/${user.Id}`}>{user.Surname}</Link></td>
            <td>{user.PhoneNumber}</td>
            <td>{user.Email}</td>
            <td>
                <Button color="success" size="sm" >Edit</Button>
                <Button onClick={() =>{deleteLandlord(user.Id)}} color="danger" size="sm">Delete</Button>            </td>            
          </tr>
        )
      });

        return(
            <div className="Landlords">
                 <h2>Landlords:</h2>
                <Table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
            {landlordsList}
            
                </tbody>
                </Table>
                <div >
             
                </div>
                <div>
                <label for="Landlords">Landlords per page:  </label>
                    <select  value={itemsPerPage} onChange={setItemsPerPageChangeHandle} name="Landlords" id="LandlordsPager" >
                        <option select="selected" value="3" name = "3" id="option1">3</option>
                        <option value="6" name = "6" id="option2">6</option>
                        <option value="9"name = "9" id="option3">9</option>
                    </select>
                    
          </div>
          <div>
          <div style={{ display: 'block', padding: 30 }}>
      
          <Pagination  count={setNumberOfPages()} defaultPage={1} page={pageNumber} onChange={handleChange} />
      </div>
          </div>
            </div>
              
           
        )
    }



export default Landlords;