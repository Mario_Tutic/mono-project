﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Booking.Model;
using Booking.Model.Common;
using Booking.Repository.Common;

namespace Booking.Repository
{
    class ReviewRepository : IReviewRepository
    {
        public async Task<List<IReview>> GetAllReviewsAsync()
        {
            SqlConnection connection = new SqlConnection("Server = localhost; Database = Booking; Trusted_Connection = True;");
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                "SELECT * FROM Review", connection);
                SqlDataReader reader = await command.ExecuteReaderAsync();
                List<IReview> reviews = new List<IReview>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var tempReview = new Review();
                        tempReview.ID = Convert.ToInt32(reader["ID"]);
                        tempReview.Created = Convert.ToDateTime(reader["Created"]);
                        tempReview.Updated = Convert.ToDateTime(reader["Updated"]);
                        tempReview.ReviewText = (string)reader["ReviewText"];
                        tempReview.ApartmentID = Convert.ToInt32(reader["ApartmentID"]);
                        tempReview.TenantID = Convert.ToInt32(reader["TenantID"]);
                        reviews.Add(tempReview);
                    }
                    reader.Close();
                    connection.Close();
                    return reviews;
                }
                else
                {
                    Console.WriteLine("No rows found.");
                    reader.Close();
                    connection.Close();
                    return null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public async Task<IReview> GetReviewByIdAsync(int id)
        {
            SqlConnection connection = new SqlConnection("data source=.\\SQLEXPRESS;Database=Booking;integrated security=SSPI");
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                "SELECT * FROM Review WHERE ID=" + id, connection);
                SqlDataReader reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    Review review = new Review();
                    while (reader.Read())
                    {
                        review.ID = Convert.ToInt32(reader["ID"]);
                        review.Created = Convert.ToDateTime(reader["Created"]);
                        review.Updated = Convert.ToDateTime(reader["Updated"]);
                        review.ReviewText = (string)reader["ReviewText"];
                        review.ApartmentID = Convert.ToInt32(reader["ApartmentID"]);
                        review.TenantID = Convert.ToInt32(reader["TenantID"]);
                    }
                    reader.Close();
                    connection.Close();
                    return review;
                }
                else
                {
                    Console.WriteLine("No rows found.");
                    reader.Close();
                    connection.Close();
                    return null;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public async Task DeleteReviewAsync(int id)
        {
            try
            {
                SqlConnection connection = new SqlConnection("data source=.\\SQLEXPRESS;Database=Booking;integrated security=SSPI");
                connection.Open();
                SqlCommand command = new SqlCommand("DELETE FROM Review WHERE ID=" + id + ";", connection);
                await command.ExecuteNonQueryAsync();
                connection.Close();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public async Task PostNewReviewAsync(IReview NewReview)
        {
            try
            {
                SqlConnection connection = new SqlConnection("data source=.\\SQLEXPRESS;Database=Booking;integrated security=SSPI");
                connection.Open();
                SqlCommand command = new SqlCommand("INSERT INTO Review VALUES(CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '"+ NewReview.ReviewText + "',"+ NewReview.TenantID + ","+ NewReview.ApartmentID + ")", connection);
                await command.ExecuteNonQueryAsync();
                connection.Close();
                return;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public async Task PutReviewAsync(string NewReviewText, int id)
        {
            try
            {
                SqlConnection connection = new SqlConnection("data source=.\\SQLEXPRESS;Database=Booking;integrated security=SSPI");
                connection.Open();
                SqlCommand command = new SqlCommand("UPDATE Review SET ReviewText = '"+ NewReviewText + "' WHERE ID = "+id+";", connection);
                await command.ExecuteNonQueryAsync();
                connection.Close();
                return;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}
