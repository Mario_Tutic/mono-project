﻿using Booking.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Repository.Common
{
    public interface IReviewRepository
    {
        Task<List<IReview>> GetAllReviewsAsync();
        Task<IReview> GetReviewByIdAsync(int id);
        Task DeleteReviewAsync(int id);
        Task PostNewReviewAsync(IReview NewReview);
        Task PutReviewAsync(string NewReviewText, int id); 
    }
}
