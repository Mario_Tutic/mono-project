﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using Booking.Repository.Common;
using Booking.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Service
{
    class BookingService : IBookingService
    {
        public BookingService(IBookingRepository Repository)
        {
            this.Repository = Repository;
        }
        protected IBookingRepository Repository { get; set; }
        public async Task<List<Model.Common.IOnlineBooking>> GetAllBookingsAsync(Sorting sort, Pager page, Filtering filter)
        {
            return await Repository.GetAllBookingsAsync(sort, page, filter);
        }

        public async Task<IOnlineBooking> GetBookingByIdAsync(int id)
        {
            return await Repository.GetBookingByIdAsync(id);
        }

        public async Task DeleteBookingAsync(int id)
        {
            await Repository.DeleteBookingAsync(id);
            return;
        }

        public async Task PostNewBookingAsync(IOnlineBooking NewBooking)
        {
            await Repository.PostNewBookingAsync(NewBooking);
        }

        public async Task PutBookingAsync(IOnlineBooking NewBooking, int ID)
        {
            await Repository.PutBookingAsync(NewBooking,ID);
        }
    }
}
