﻿using Booking.Common;
using Booking.Model;
using Booking.Model.Common;
using Booking.Service;
using Booking.Service.Common;
using Booking.WebApi.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;


namespace Booking.WebApi.Controllers
{
    public class ApartmentController : ApiController
    {
        protected IApartmentService service { get; private set; }
        private readonly IMapper mapper;

        public ApartmentController(IApartmentService _service, IMapper _mapper)
        {
            service = _service;
            mapper = _mapper;
        }

        public Sorting sort = new Sorting();
        public Pager page = new Pager();
        public Filtering filter = new Filtering();

        [HttpGet]
        public async Task<HttpResponseMessage> GetAllApartmentAsync(string sortBy = "", string orderBy = "", int itemsPerPage = 0, int pageNumber = 0, int price = 0, int rooms = 0)
        {
            if (sortBy != "" || orderBy != "")
            {
                sort = new Sorting();
                sort.OrderBy = orderBy;
                sort.SortOrder = sortBy;
            }

            if (pageNumber != 0 || itemsPerPage != 0)
            {
                page = new Pager();
                page.ItemsPerPage = itemsPerPage;
                page.PageNumber = pageNumber;
            }

            if (price != 0 || rooms != 0)
            {
                filter.Price = price;
                filter.Rooms = rooms;
            }

            List<IApartment> apartmentList = await service.GetAllApartmentsAsync(sort, page, filter);

            if (apartmentList != null)
            {
                List<ApartmentViewModel> apartmentViewList = new List<ApartmentViewModel>();

                foreach (Apartment apartment in apartmentList)
                {
                    var apartmentViewModel = mapper.Map<ApartmentViewModel>(apartment);
                    apartmentViewList.Add(apartmentViewModel);
                }

                return Request.CreateResponse(HttpStatusCode.OK, apartmentViewList);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, "There are no such apartmants currently listed");
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetApartmentByIdAsync(int id)
        {
            IApartment apartment = await service.GetApartmentByIdAsync(id);

            if (apartment.Price != 0)
            {
                var apartmentViewModel = mapper.Map<ApartmentViewModel>(apartment);

                return Request.CreateResponse(HttpStatusCode.OK, apartmentViewModel);
            }
                
            return Request.CreateResponse(HttpStatusCode.NotFound, "There is no such apartmant");
        }

        [HttpPost]
        public async Task<HttpResponseMessage> SaveNewApartmentAsync([FromBody] ApartmentViewModel apartmentViewModel)
        {
            List<IApartment> apartmentList = await service.GetAllApartmentsAsync(sort, page, filter);
            IApartment newApartment = apartmentList.Where(s => s == apartmentViewModel).FirstOrDefault();

            if (newApartment.Price != 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "This apartment already exists in database");

            IApartment apartment = new Apartment();

            apartment = mapper.Map<IApartment>(apartmentViewModel);

            await service.SaveNewApartmentAsync(apartment);
            return Request.CreateResponse(HttpStatusCode.OK, "Apartment has been created");
        }

        [HttpPut]
        public async Task<HttpResponseMessage> UpdateApartmentAsync(int id, double rating, int price, int rooms, bool free)
        {
            IApartment apartment = await service.GetApartmentByIdAsync(id);

            if (apartment.Price != 0)
            {
                await service.UpdateApartmentAsync(id, rating, price, rooms, free);
                return Request.CreateResponse(HttpStatusCode.OK, "Apartment updated!");
            }

            return Request.CreateResponse(HttpStatusCode.NotFound, "Apartment not found!");
        }

        [HttpDelete]
        public async Task<HttpResponseMessage> RemoveApartmentAsync(int id)
        {
            IApartment apartment = await service.GetApartmentByIdAsync(id);

            if (apartment.Price != 0)
                return Request.CreateResponse(HttpStatusCode.OK, "Apartment deleted!");

            return Request.CreateResponse(HttpStatusCode.NotFound, "Apartment not found!");
        }
    }
}