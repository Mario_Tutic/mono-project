import React, { Component } from 'react'
import ReviewService from '../services/ReviewService'

class ListReviewsComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                reviews: []
        }
        this.addReview = this.addReview.bind(this);
        this.editReview = this.editReview.bind(this);
        this.deleteReview = this.deleteReview.bind(this);
    }

    deleteReview(id){
        ReviewService.deleteReview(id).then( res => {
            this.setState({reviews: this.state.reviews.filter(review => review.id !== id)});
            window.location.reload(false);
        });
    }
    editReview(id){
        this.props.history.push(`/update-review/${id}`);
    }

    componentDidMount(){
        ReviewService.getReviews().then((res) => {
            this.setState({ reviews: res.data});
        });
    }

    addReview(){
        this.props.history.push('/add-review');
    }

    render() {
        return (
            <div>
                 <h2 className="text-center">Review List</h2>
                 <div className = "row">
                    <button className="btn btn-primary" onClick={this.addReview}> Write new Review</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> ID</th>
                                    <th> TenantID</th>
                                    <th> ReviewText</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.reviews.map(
                                        review => 
                                        <tr key = {review.ID}>
                                             <td> {review.ID} </td>   
                                             <td> {review.TenantID}</td>
                                             <td> {review.ReviewText}</td>
                                             <td>
                                                 <button onClick={ () => this.editReview(review.ID)} className="btn btn-info">Update </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteReview(review.ID)} className="btn btn-danger">Delete </button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                 </div>

            </div>
        )
    }
}

export default ListReviewsComponent
